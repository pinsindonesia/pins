const BASE_URL = 'http://localhost:8000' 
// const BASE_URL = 'http://180.250.80.81'
// const BASE_URL =  'http://demo-pins.herokuapp.com'
// const BASE_URL =  'https://pins.co.id/pins/public'
const API_BASE_URL = BASE_URL + '/api'
const DIR_BASE_URL = BASE_URL + '/storage'
// const DIR_BASE_URL = BASE_URL + '/img'

export const Constants = {
  BASE_URL: BASE_URL,
  API_BASE_URL: API_BASE_URL,
  API_MENU_URL: API_BASE_URL + '/menu',
  API_FOOTERS_URL: API_BASE_URL + '/footers',
  API_SLIDERS_URL: API_BASE_URL + '/sliders',
  API_PRODUCTS_URL: API_BASE_URL + '/products',
  API_AREA_URL: API_BASE_URL + '/area',
  API_NEWS_LATEST_URL: API_BASE_URL + '/news/latest',
  API_NEWS_FEATURED_URL: API_BASE_URL + '/news/featured',
  API_NEWS_ALL_URL: API_BASE_URL + '/news/all',
  API_NEWS_URL: API_BASE_URL + '/news/',
  API_COMMENTS_URL: API_BASE_URL + '/comments',
  API_ENQUIRY_URL: API_BASE_URL + '/contacts',
  API_ANALYTIC_URL: API_BASE_URL + '/analytics',
  API_USER_URL: API_BASE_URL + '/users',
  API_TEAMS_URL: API_BASE_URL + '/teams',
  YOUTUBE_BASE_WATCH_URL: 'https://www.youtube.com/watch?v=',
  YOUTUBE_BASE_EMBED_URL: 'https://www.youtube.com/embed',

  DIR_MENU_URL: DIR_BASE_URL + '/menu_icon',
  DIR_NEWS_URL: DIR_BASE_URL + '/article',
  DIR_PRODUCT_URL: DIR_BASE_URL + '/product',
  DIR_CONTENT_URL: DIR_BASE_URL + '/content',
  DIR_COMPANY_URL: DIR_BASE_URL + '/company',
  DIR_PARTNER_URL: DIR_BASE_URL + '/partner',
  DIR_SOCIAL_MEDIA_URL: DIR_BASE_URL + '/socialmedia',
  DIR_TEAM_URL: DIR_BASE_URL + '/team',

  IMG_COVER_DEFAULT: '/img/frontend/default_cover_image.jpg'

}
