export default function reducer(state={
    footer: {},
    fetching: false,
    fetched: false,
    error: null
}, action) {
    switch (action.type) {
        case 'FETCH_FOOTER': {
            return {...state, fetching: true}
        }
        case 'FETCH_FOOTER_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }
        case 'FETCH_FOOTER_FULFILLED': {
            return {...state, fetching: false, fetched: true, footer: action.payload}
        }
    }

    return state
}
