<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PINS INDONESIA - The IoT Company</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{asset('/frontend/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('/frontend/lib/nivo-slider/css/nivo-slider.css')}}" rel="stylesheet">
        <link href="{{asset('/frontend/lib/owlcarousel/owl.carousel.min.css')}}" rel="stylesheet">
        <link href="{{asset('/frontend/lib/owlcarousel/owl.theme.default.min.css') }}" rel="stylesheet">
        <link href="{{asset('/frontend/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{asset('/frontend/lib/animate/animate.min.css') }}" rel="stylesheet">
        <link href="{{asset('/frontend/lib/venobox/venobox.css') }}" rel="stylesheet">
        <link href="{{asset('/css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app"></div>
        <script src="{{asset('/frontend/lib/wow/wow.min.js')}}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1hmrtC0PDyM5NtWKneEXV4TfLKK6GRR8"></script>
        <script crossorigin src="https://unpkg.com/universal-cookie@3/umd/universalCookie.min.js"></script>
        <script src="{{asset('/js/app.js')}}"></script>
    </body>
</html>
