import Axios from 'axios'
import { Constants, Helpers } from '../../utils';

export function fetchNewsLatest() {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_NEWS_LATEST_URL))
        .then(res => {
            dispatch({ type: 'FETCH_NEWS_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_NEWS_REJECTED', payload: err })
        })
    }
}

export function fetchNewsAll() {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_NEWS_ALL_URL))
        .then(res => {
            dispatch({ type: 'FETCH_NEWS_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_NEWS_REJECTED', payload: err })
        })
    }
}

export function fetchSingleNews(id) {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_NEWS_URL+id))
        .then(res => {
            dispatch({ type: 'FETCH_SINGLE_NEWS_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_SINGLE_NEWS_REJECTED', payload: err })
        })
    }
}
