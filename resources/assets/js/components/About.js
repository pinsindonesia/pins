import React, { Component } from 'react'
import { connect } from 'react-redux'
import Tilt from 'react-tilt'

import { Helpers } from '../utils';

import SectionHeadline from './SectionHeadline'

@connect((store) => {
    return {
        menuDetail: store.menu.menuDetail,
        fetched: store.menu.mdFetched
    }
})
export default class About extends Component {
    render() {
        const {fetched, menuDetail} = this.props
        let name = 'PINS Indonsia',
            desc = '',
            image = ''

        if (fetched) {
            name = menuDetail.company.name
            desc = menuDetail.company.description
            image = Helpers.imgUrlCompany(menuDetail.company.image)
        }

        return (
            <div id='about' className='about-area area-padding'>
                <div className='container'>
                <SectionHeadline title={name} />

                <div className='row'>

                    <div className='col-md-6 col-sm-6 col-xs-12 wow fadeInLeft'>
                        <div className='well-left'>
                            <Tilt className='single-well' options={{scale:1.01}}>
                                <img className='two-side-rounded-fixed' src={image} alt=''/>
                            </Tilt>
                        </div>
                    </div>

                    <div className='col-md-6 col-sm-6 col-xs-12 wow fadeInRight'>
                        <div className='well-middle'>
                            <div className='single-well'>
                                <a href='#'><h4 className='sec-head'>The IoT Company</h4></a>
                                <p>{desc}</p>
                                <ul>
                                    <li><i className='fa fa-check'></i> IoT Service</li>
                                    <li><i className='fa fa-check'></i> Mobility Service</li>
                                    <li><i className='fa fa-check'></i> CPE Manage Service</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                </div>
            </div>
        );
    }
}
