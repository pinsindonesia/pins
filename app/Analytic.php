<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Analytic extends Model
{
    protected $fillable = [
        'url', 'useragent'
    ];
    protected $table = 'analytics';
}
