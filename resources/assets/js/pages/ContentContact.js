import React, { Component } from 'react'
import { connect } from 'react-redux'

import ImageHeadline from '../components/ImageHeadline'

import { setActiveMenu } from '../redux/actions/menuActions'
import Contact from '../components/Contact';
import { fetchArea } from '../redux/actions/areaActions';
import { Helpers } from '../utils';

@connect((store) => {
    return {
        active: store.menu.active,
        menuDetail: store.menu.menuDetail,
        fetched: store.menu.mdFetched && store.footer.fetched,
        footer: store.footer.footer,
        area: store.area.area,
        areaFetched: store.area.fetched,
    }
})
export default class ContentContact extends Component {
    componentWillMount() {
        this.props.dispatch(fetchArea())
    }

    componentDidMount() {
        this.props.dispatch(setActiveMenu(this.props.href))
    }

    render() {
        const {fetched, menuDetail, areaFetched, area, footer} = this.props

        let comp = <div id='preloader'></div>

        if (fetched) {
            const { title } = menuDetail
            const { phone, mail, address } = footer.contact

            let areaComp = <div className='area row'>
                <div className='col-md-4 col-sm-4 col-xs-12'>
                    <div className='area-item'>
                        <h1 className='section-title'>{Helpers.localeText('main_office')}</h1>
                        <div className='content'>{address}</div>
                        <div className='content'>{Helpers.localeText('phone_short')} {phone}</div>
                        <div className='content'>{Helpers.localeText('email')}: {mail}</div>
                    </div>

                    {areaFetched && area.map((ar, id) => {
                        if (ar.featured) return <div key={id} className='area-item'>
                            <h1 className='section-title'>{ar.name}</h1>
                            <div className='content'>{ar.address}</div>
                        <div className='content'>{Helpers.localeText('phone_short')} {ar.phone}</div>
                        <div className='content'>{Helpers.localeText('email')}: {ar.email}</div>
                        </div>
                    })}
                </div>
                <div className='col-md-8 col-sm-8 col-xs-12'>
                    <div className='area-item'>
                        <h1 className='section-title'>{Helpers.localeText('coverage_area')}</h1>
                    </div>

                    <ol className='row'>{areaFetched && area.map((ar, id) => {
                        if (!ar.featured) return <li key={id} className='area-item-wrapper col-md-6 col-sm-6 col-xs-12'>
                                    <div className='area-item'>
                                    <h1>{ar.name}</h1>
                                    <div className='content'>{ar.address}</div>
                                </div>
                            </li>
                    })}</ol>
                </div>
            </div>

            // if (content) {
                comp =
                    <div>
                        <ImageHeadline title='' img='/img/frontend/contact.jpg' />
                        <div className='container'>
                            <div className='row'>
                            <article className='blog-post-wrapper soft-shadow wow fadeInUp'>
                                <div className='post-information'>
                                    <Contact title={title} phone={phone} mail={mail} address={address} />

                                    {areaFetched && areaComp}
                                </div>

                            </article>

                            </div>
                        </div>
                    </div>
            // }
        }

        return comp
    }
}
