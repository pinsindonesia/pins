@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Email Template Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Content
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Update!-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Email Template</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_edit' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group" style="display: none;">
                            <label class="control-label col-md-2">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="name_edit_old" name="name_edit_old" type="hidden" />
                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control" disabled="disabled" />
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="control-label col-md-2">Type<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="type_edit_old" name="type_edit_old" type="hidden"/>
                                <select class="form-control" id="type_edit" name="type_edit" onchange="type_variable();">
                                    <option value="">-- choose type --</option>
                                    <option value="1">Contact Us</option>
                                    <option value="2">Comment</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Subject<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="subject_edit_old" name="subject_edit_old" type="hidden" />
                                <input id="subject_edit" name="subject_edit" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Content<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="content_edit_old" name="content_edit_old" type="hidden" />
                                <textarea id="content_edit" ncontent_long_edit" type="text" data-required="1" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="form-group type_variable_edit" id="type_variable_contact_edit" style="color: gray; display: none; margin-left: 10px;">
                            <span class="col-md-offset-2"><span class="required">* </span>these variables in content will be changed to sender information</span>
                            <ul class="col-md-offset-2" style="list-style: none;">
                                <li>**name**</li>
                                <li>**phone**</li>
                                <li>**email**</li>
                                <li>**message**</li>
                            </ul>
                        </div>
                        <div class="form-group type_variable_edit" id="type_variable_comment_edit" style="color: gray; display: none; margin-left: 10px;">
                            <span class="col-md-offset-2"><span class="required">* </span>these variables in content will be changed to sender information</span>
                            <ul class="col-md-offset-2" style="list-style: none;">
                                <li>**name**</li>
                                <li>**email**</li>
                                <li>**comment**</li>
                                <li>**article**</li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden" />
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    var validator_edit;

    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "paging":   false,
            "info":     false,
            "searching": false,
            "ajax": {
                'type': 'GET',
                'url': 'emailtemplate/list'
            },
            "columnDefs": [{
                "targets": 2,
                "className": "dt-center"
            }, {
                "targets": [1,2],
                "orderable": false
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth": "20%"}, {"sWidth": "76%"}, {"sWidth": "4%"}]
        });
    });

    function ajaxReload() {
        $layout = $('#modal_add #layout_add').val();
    }

    function type_variable() {
        $type = $('#modal_edit #type_edit').val();

        $('#modal_edit .type_variable_edit').hide();

        if($type == 1) {
            $('#modal_edit #type_variable_contact_edit').show();
        } else if($type == 2) {
            $('#modal_edit #type_variable_comment_edit').show();
        }
    }

    function submitForm() { 
        $id                 = $('#update_id').val();
        $type               = $('#modal_edit #type_edit').val();
        $name               = $('#modal_edit #name_edit').val();
        $subject            = $('#modal_edit #subject_edit').val();
        $content            = $('#modal_edit #content_edit').val();

        $type_old               = $('#modal_edit #type_edit_old').val();
        $name_old               = $('#modal_edit #name_edit_old').val();
        $subject_old            = $('#modal_edit #subject_edit_old').val();
        $content_old            = $('#modal_edit #content_edit_old').val();

        if($type == $type_old && $name == $name_old && $subject == $subject_old && $content == $content_old) {
            $('#confirm_modal_edit').modal('toggle');
            $('#modal_edit').modal('toggle');
        } else {
            $(".loading").show();

            var form = $('#form_edit')[0];
            var fdata = new FormData(form);

            $content  = $content == '<div><br></div>' || $content == '<br>' ? '' : $content;

            fdata.append("id", $id);
            fdata.append("type", $type);
            fdata.append("name", $name);
            fdata.append("subject", $subject);
            fdata.append("content", $content);

            $.ajax({
                type: 'POST',
                url: "emailtemplate/update", 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fdata, 
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.status == 'success') {
                        $('#confirm_modal_edit').modal('toggle');
                        $('#modal_edit').modal('toggle');

                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                    } else {
                        $('#confirm_modal_edit').modal('toggle');

                        $('#modal_edit').animate({ scrollTop: 0 }, 'slow');

                        $("#modal_message_edit").html(response.message);
                        $("#modal_message_edit").show();
                        setTimeout(function() { $("#modal_message_edit").slideUp(); }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        }
    }

    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "emailtemplate/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#modal_edit #content_edit').unRichText();

                $('#modal_edit #content_edit').val('');

                $('#update_id').val(response.id);
                $('#modal_edit #type_edit').val(response.type);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #subject_edit').val(response.subject);
                $('#modal_edit #content_edit').val(response.content);
                
                $('#modal_edit #type_edit_old').val(response.type);
                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #subject_edit_old').val(response.subject);
                $('#modal_edit #content_edit_old').val(response.content);

                $('#modal_edit #content_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 200,
                      videoEmbed: false,
                });

                type_variable();

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Edit Validation
    $().ready(function() {
        validator_edit = $("#form_edit").validate({
            rules: {
                type_edit: {
                    required: true
                },
                name_edit: {
                    required: true
                },
                subject_edit: {
                    required: true
                },
                content_edit: {
                    required: true
                }
            },
            messages: {
                type_edit: {
                    required: "Please choose a type"
                },
                name_edit: {
                    required: "Please enter a name"
                },
                subject_edit: {
                    required: "Please enter a subject"
                },
                content_edit: {
                    required: "Please enter a content"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });

</script>
<style>
    
#modal_edit {
  overflow: auto !important;
}

</style>
@endsection
