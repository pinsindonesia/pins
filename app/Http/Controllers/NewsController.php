<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Article;
use App\Helpers;

class NewsController extends Controller
{
    public function show(Request $request, $query)
    {
        $locale = $request->query('locale');

        $article = Article::find($query);

        if (empty($article)) {
            return response()->json(['message' => 'news not found'], 404);
        } else {
            $data = [
                'title' => Helpers::getLocale($locale, $article->title_id, $article->title_en),
                'img' => 'http://180.250.80.81/storage/article/'.$article->image,
                'desc' => strip_tags(Helpers::getLocale($locale, $article->description_short_id, $article->description_short_en)),
                'url' => 'http://180.250.80.81/n/'.$query.'/#/news/'.$query,
            ];
            return view('news')->with($data);
        }

    }
}
