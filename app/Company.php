<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'name', 'email', 'phone', 'email_notif', 'address', 'image', 'description' 
    ];

    protected $dates = ['deleted_at'];
    protected $table = 'companies';
}
