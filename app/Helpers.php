<?php

namespace App;

class Helpers
{
    const MENU_TYPE_OTHER   = 0;
    const MENU_TYPE_HOME    = 1;
    const MENU_TYPE_PRODUCT = 2;
    const MENU_TYPE_NEWS    = 3;
    const MENU_TYPE_TEAM    = 4;
    const MENU_TYPE_PARTNER = 5;
    const MENU_TYPE_CONTACT = 6;
    const MENU_TYPE_ABOUT = 7;

    public static function getLocale($locale, $value_id, $value_en) {
        return ((strcasecmp($locale, 'en') == 0) ? ''.$value_en : ''.$value_id);
    }

    public static function articleToNews($locale, $article) {
        return [
            'id' => $article->id,
            'title' => Helpers::getLocale($locale, $article->title_id, $article->title_en),
            'content' => Helpers::getLocale($locale, $article->description_short_id, $article->description_short_en),
            'allContent' => Helpers::getLocale($locale, $article->description_long_id, $article->description_long_en),
            'img' => $article->image,
            'comments' => count($article->comments()->showed()->get()),
            'date' => $article->created_at->toIso8601ZuluString()
        ];
    }

    public static function genContent($locale, $content) {
        if ($content != null) {
            return [
                'layout' => $content->layout,
                'text_1' => Helpers::getLocale($locale, $content->text_1_id, $content->text_1_en),
                'text_2' => Helpers::getLocale($locale, $content->text_2_id, $content->text_2_en),
                'text_3' => Helpers::getLocale($locale, $content->text_3_id, $content->text_3_en),
                'text_4' => Helpers::getLocale($locale, $content->text_4_id, $content->text_4_en),
                'image_1' => Helpers::fixNull($content->image_1),
                'image_2' => Helpers::fixNull($content->image_2),
                'image_3' => Helpers::fixNull($content->image_3),
                'image_4' => Helpers::fixNull($content->image_4),
                'video_1' => Helpers::fixNull($content->video_1),
                'video_2' => Helpers::fixNull($content->video_2)
            ];
        } else {
            return null;
        }
    }

    public static function fixNull($content) {
        return is_null($content) ? '' : $content;
    }

    public static function getClientIp() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public static function getUserAgent() {
        $useragent = '';
        if (isset($_SERVER['USER_AGENT']))
            $useragent = $_SERVER['USER_AGENT'];
        else if(isset($_SERVER['HTTP_USER_AGENT']))
            $useragent = $_SERVER['HTTP_USER_AGENT'];
        else
            $useragent = 'UNKNOWN';
        return $useragent;
    }

    public static function genMailContact($content, $enquiry) {
        $data = str_replace("**name**", $enquiry->name, $content);
        $data = str_replace("**email**", $enquiry->email, $data);
        $data = str_replace("**phone**", $enquiry->phone, $data);
        $data = str_replace("**message**", $enquiry->message, $data);
        return $data;
    }

    public static function genMailComment($content, $comment) {
        $data = str_replace("**name**", $comment->name, $content);
        $data = str_replace("**email**", $comment->email, $data);
        $data = str_replace("**article**", $comment->article->title_id." / ".$comment->article->title_en, $data);
        $data = str_replace("**comment**", $comment->comment, $data);
        return $data;
    }
}
