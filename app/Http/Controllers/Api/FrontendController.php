<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Analytic;
use App\Area;
use App\Article;
use App\Comment;
use App\Company;
use App\Content;
use App\Enquiry;
use App\Emailtemplate;
use App\Mail\MailTemplate;
use App\Menu;
use App\Partner;
use App\Product;
use App\Socialmedia;
use App\Team;
use App\Helpers;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class FrontendController extends Controller
{
    public function showSliders(Request $request) {
        $locale = $request->query('locale');

        $data = [];

        foreach (Product::featuredSaved()->get() as $value) {
            $menu = $value->menu->first();
            if ($menu != null && $menu->link != null) {
                array_push($data, [
                    'key' => $menu->link,
                    'img' => $value->image,
                    'title' => Helpers::getLocale($locale, $menu->name_id, $menu->name_en),
                    'subtitle' => $value->excerpt
                ]);
            }
        }

        return response()->json($data);
    }

    public function showMenu(Request $request) {
        $locale = $request->query('locale');

        $data = [];

        foreach (Menu::whereNull('parent_id')->orderBy('order', 'asc')->get() as $value) {
            $data_child = [];

            foreach (Menu::find($value->id)->child()->orderBy('order', 'asc')->get() as $nValue) {
                $data_g_child = [];

                foreach (Menu::find($nValue->id)->child()->orderBy('order', 'asc')->get() as $nnValue) {
                    $name = Helpers::getLocale($locale, $nnValue->name_id, $nnValue->name_en);
                    array_push($data_g_child, [
                        'title' => $name,
                        'type' => $nnValue->type,
                        'href' => $nnValue->link
                    ]);
                }

                $name = Helpers::getLocale($locale, $nValue->name_id, $nValue->name_en);
                array_push($data_child, [
                    'title' => $name,
                    'type' => $nValue->type,
                    'href' => $nValue->link,
                    'icon' => $nValue->icon,
                    'child' => $data_g_child
                ]);
            }

            $name = Helpers::getLocale($locale, $value->name_id, $value->name_en);
            array_push($data, [
                'id' => $value->id,
                'title' => $name,
                'type' => $value->type,
                'href' => $value->link,
                'child' => $data_child
            ]);
        }

        return response()->json($data);
    }

    public function showMenuDetails(Request $request, $query) {
        $locale = $request->query('locale');

        $data = [];
        $menu = Menu::where('link', $query)->first();

        if ($menu != null) {
            $data_child = [];
            $child = $menu->child;

            foreach ($menu->child as $value) {
                $data_g_child = [];

                foreach (Menu::find($value->id)->child as $nValue) {
                    $name = Helpers::getLocale($locale, $nValue->name_id, $nValue->name_en);
                    array_push($data_g_child, [
                        'title' => $name,
                        'type' => $nValue->type,
                        'href' => $nValue->link
                    ]);
                }

                $name = Helpers::getLocale($locale, $value->name_id, $value->name_en);
                $product = Product::find($value->ref_id);
                array_push($data_child, [
                    'title' => $name,
                    'type' => $value->type,
                    'filter' => $value->link,
                    'parent' => Helpers::getLocale($locale, $menu->name_id, $menu->name_en),
                    'img' => $product->image,
                    'icon' => $value->icon,
                    'href' => $value->link,
                    'child' => $data_g_child
                ]);
            }

            $name = Helpers::getLocale($locale, $menu->name_id, $menu->name_en);
            $data = [
                'id' => $menu->id,
                'title' => $name,
                'type' => $menu->type,
                'href' => $menu->link,
                'ref' => $menu->ref_id,
                'child' => $data_child
            ];

            if ($menu->parent_id != null) {
                $data['href'] = $menu->parent->link;
                if ($menu->parent->parent_id != null) {
                    $data['href'] = $menu->parent->parent->link;
                }
            }

            if ($menu->type == Helpers::MENU_TYPE_PRODUCT) {
                $product = Product::find($menu->ref_id);

                if ($product != null) {
                    $data['product'] = [
                        'id' => $product->id,
                        'title' => $product->title,
                        'image' => $product->image,
                        'featured' => $product->featured == 'true' ? true : false,
                    ];

                    if ($product->featured == 'false') {
                        $data['product']['overviewContent'] = Helpers::genContent($locale, $product->overview);
                        $data['product']['whyUsContent'] = Helpers::genContent($locale, $product->whyUs);
                        $data['product']['productContent'] = Helpers::genContent($locale, $product->product);
                        $data['product']['casedContent'] = Helpers::genContent($locale, $product->cased);
                    } else if ($product->isFeaturedSaved == 'true') {
                        $data['product']['overviewContent'] = Helpers::genContent($locale, $product->overview);

                        $c_partner = [];

                        foreach ($product->partner()->get() as $p_item) {
                            array_push($c_partner, [
                                'id' => $p_item->id,
                                'title' => $p_item->name,
                                'img' => $p_item->image,
                                'desc' => $p_item->description,
                            ]);
                        }

                        $data['product']['partner'] = $c_partner;
                    }
                }
            }

            if ($menu->type == Helpers::MENU_TYPE_HOME) {
                $company = Company::first();

                if ($company != null) {
                    $data['company'] = [
                        'name' => $company->name,
                        'description' => $company->description,
                        'image' => $company->image,
                    ];
                }
            }

            if ($menu->type == Helpers::MENU_TYPE_OTHER) {
                $content = Content::find($menu->ref_id);

                if ($content != null) {
                    $data['content'] = Helpers::genContent($locale, $content);
                }
            }

            if ($menu->type == Helpers::MENU_TYPE_TEAM) {
                $team = [];

                foreach (Team::orderBy('group_name', 'asc')->get() as $t) {

                    array_push($team, [
                        'id' => $t->id,
                        'name' => $t->name,
                        'title' => Helpers::getLocale($locale, $t->title_id, $t->title_en),
                        'photo' => $t->photo,
                        'group' => $t->group_name,
                        'details' => Helpers::getLocale($locale, $t->details_id, $t->details_en)
                    ]);
                }

                $data['teams'] = $team;
            }

            if ($menu->type == Helpers::MENU_TYPE_PARTNER) {
                $partner = [];

                foreach (Product::featuredSaved()->get() as $product) {
                    $c_partner = [];

                    foreach ($product->partner()->get() as $p_item) {
                        array_push($c_partner, [
                            'id' => $p_item->id,
                            'title' => $p_item->name,
                            'img' => $p_item->image,
                            'desc' => $p_item->description,
                        ]);
                    }

                    $p_menu = $product->menu->first();
                    array_push($partner, [
                        'key' => $p_menu->link,
                        'img' => $product->image,
                        'title' => Helpers::getLocale($locale, $p_menu->name_id, $p_menu->name_en),
                        'subtitle' => $product->excerpt,
                        'child' => $c_partner
                    ]);
                }

                $data['partner'] = $partner;
            }

            return response()->json($data);
        } else {
            return response()->json(['message' => 'menu not found'], 404);
        }

    }

    public function showProduct(Request $request) {
        $locale = $request->query('locale');

        $parent = [];
        $child = [];

        foreach (Menu::featuredSaved()->get() as $value) {
            $all_child = Menu::find($value->id)->child()->orderBy('order', 'asc')->get();
            if (!$all_child->isEmpty()) {

                foreach ($all_child as $nValue) {
                    $product = Product::find($nValue->ref_id);
                    $ok = '-';
                    if ($product && $product->image) $ok = $product->image;

                    $d = [
                        'title' => Helpers::getLocale($locale, $nValue->name_id, $nValue->name_en),
                        'filter' => $value->link,
                        'parent' => Helpers::getLocale($locale, $value->name_id, $value->name_en),
                        'href' => $nValue->link,
                        'img' => $ok,
                        'icon' => $nValue->icon
        ];

                    array_push($child, $d);
                }

                array_push($parent, [
                    'title' => Helpers::getLocale($locale, $value->name_id, $value->name_en),
                    'filter' => $value->link]);
            }
        }

        return response()->json([
            'parent' => $parent,
            'child' => $child
        ]);
    }

    public function showProductDetails(Request $request, $query) {
        $locale = $request->query('locale');

        $product = Product::find($query);

        if ($product != null) {
            return response()->json([
                'id' => $product->id,
                'title' => $product->title,
                'overviewContent' => $product->overview,
                'whyUsContent' => $product->whyUs,
                'productContent' => $product->product,
                'casedContent' => $product->cased,
                'image' => $product->image,
            ]);
        } else {
            return response()->json(['message' => 'product not found'], 404);
        }
    }

    public function showFooter(Request $request) {
        $locale = $request->query('locale');

        $menu = [];

        foreach (Menu::getFooter() as $value) {
            $all_child = Menu::find($value->id)->child()->orderBy('order', 'asc')->get();
            if (!$all_child->isEmpty()) {
                $data_child = [];

                    foreach ($all_child as $nValue) {
                    $name = Helpers::getLocale($locale, $nValue->name_id, $nValue->name_en);
                    array_push($data_child, [
                        'title' => $name,
                        'type' => $nValue->type,
                        'href' => $nValue->link
                    ]);
                }

                $name = Helpers::getLocale($locale, $value->name_id, $value->name_en);
                array_push($menu, [
                    'id' => $value->id,
                    'title' => $name,
                    'type' => $value->type,
                    'href' => $value->link,
                    'child' => $data_child
                ]);
            }
        }

        $sm = [];
        foreach (Socialmedia::all() as $value) {
            array_push($sm, [
                'title' => strtolower($value->name),
                'href' => $value->link,
            ]);
        }

        $data = [
            'socialMedia' => $sm,
            'menus' => $menu
        ];

        $company = Company::first();
        if ($company) {
            $data['contact'] = [
                'phone' => $company->phone,
                'mail' => $company->email,
                'address' => $company->address,
            ];
        }

        return response()->json($data);
    }

    public function showNews(Request $request, $query) {
        $locale = $request->query('locale');

        if (strcasecmp($query, 'latest') == 0) {
            $news = [];
            foreach (Article::latest()->limit(3)->get() as $article) {
                array_push($news, Helpers::articleToNews($locale, $article));
            }
            return response()->json($news);
        } if (strcasecmp($query, 'all') == 0) {
            $news = [];
            foreach (Article::latest()->get() as $article) {
                array_push($news, Helpers::articleToNews($locale, $article));
            }
            return response()->json($news);
        } if (strcasecmp($query, 'featured') == 0) {
            $news = [];
            foreach (Article::featured()->get() as $article) {
                array_push($news, Helpers::articleToNews($locale, $article));
            }
            return response()->json($news);
        } else {
            $d_news = Article::find($query);

            if (empty($d_news)) {
                return response()->json(['message' => 'news not found'], 404);
            } else {
                return response()->json(Helpers::articleToNews($locale, $d_news));
            }
        }
    }

    public function showComments(Request $request, $id) {
        $article = Article::find($id);

        if (empty($article)) {
            return response()->json(['message' => 'articles not found'], 404);
        } if ($article->comments()->showed()->get()->isEmpty()) {
            return response()->json(['message' => 'comments not found'], 404);
        } else {
            $comments = [];

            foreach ($article->comments()->showed()->get() as $cm) {
                array_push($comments, [
                    'author' => $cm->name,
                    'email' => $cm->email,
                    'date' => $cm->created_at->toIso8601ZuluString(),
                    'comment' => $cm->comment
                ]);
            }

            return response()->json($comments);
        }
    }

    public function showArea(Request $request) {
        $locale = $request->query('locale');

        $data = [];

        foreach (Area::get() as $value) {
            array_push($data, [
                'name' => $value->name,
                'address' => $value->location,
                'phone' => $value->telp,
                'fax' => $value->fax,
                'email' => $value->email,
                'featured' => ($value->featured == 'true') ? true : false,
            ]);
        }

        return response()->json($data);
    }

    public function addComment(Request $request) {
        $data = $request->json()->all();

        if (!isset($data['author']) || !isset($data['email']) || !isset($data['comment']) || !isset($data['article_id'])) {
            return response()->json(['message' => 'cannot add comment'], 500);
        } else {
            $comment = new Comment([
                'name' => $data['author'],
                'email' => $data['email'],
                'comment' => $data['comment'],
                'article_id' => $data['article_id']
            ]);

            if ($comment->save()) {
                $tAdmin = Emailtemplate::commentAdmin()->first();
                $tUser = Emailtemplate::commentUser()->first();
                $company = Company::first();

                if ($tAdmin != null && $tUser != null) {
                    $dAdmin = Helpers::genMailComment($tAdmin->content, $comment);
                    $dUser = Helpers::genMailComment($tUser->content, $comment);

                    if ($company != null && $company->email_notif != null) {
                        Mail::to($company->email_notif)->send(new MailTemplate($tAdmin->subject, $dAdmin));
                    }

                    Mail::to($comment->email)->send(new MailTemplate($tUser->subject, $dUser));
                }

                return response()->json($comment);
            } else {
                return response()->json(['message' => 'cannot add comment'], 500);
            }
        }
    }

    public function addEnquiry(Request $request) {
        $data = $request->json()->all();

        if (!isset($data['name']) || !isset($data['email']) || !isset($data['message']) || !isset($data['phone'])) {
            return response()->json(['message' => 'cannot add enquiry'], 500);
        } else {
            $enquiry = new Enquiry([
                'name' => $data['name'],
                'email' => $data['email'],
                'message' => $data['message'],
                'phone' => $data['phone']
            ]);

            if ($enquiry->save()) {
                $tAdmin = Emailtemplate::contactUsAdmin()->first();
                $tUser = Emailtemplate::contactUsUser()->first();
                $company = Company::first();

                if ($tAdmin != null && $tUser != null) {
                    $dAdmin = Helpers::genMailContact($tAdmin->content, $enquiry);
                    $dUser = Helpers::genMailContact($tUser->content, $enquiry);

                    if ($company != null && $company->email_notif != null) {
                        Mail::to($company->email_notif)->send(new MailTemplate($tAdmin->subject, $dAdmin));
                    }
                    Mail::to($enquiry->email)->send(new MailTemplate($tUser->subject, $dUser));
                }

                return response()->json($enquiry);
            } else {
                return response()->json(['message' => 'cannot add enquiry'], 500);
            }
        }
    }

    public function addAnalytics(Request $request) {
        $data = $request->json()->all();

        if (!isset($data['url']) || !isset($data['useragent'])) {
            return response()->json(['message' => 'error'], 500);
        } else {
            $analytic = new Analytic([
                'url' => $data['url'],
                'useragent' => $data['useragent'].';;'.Helpers::getClientIp().';;'.Helpers::getUserAgent()
            ]);

            if ($analytic->save()) {
                return response()->json($analytic);
            } else {
                return response()->json(['message' => 'error'], 500);
            }
        }
    }
}
