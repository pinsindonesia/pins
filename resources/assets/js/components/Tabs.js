import React, { Component } from 'react'

export class TabItem extends Component {
    render() {
        const { active, tabkey, title, letterSpacing } = this.props

        return (
            <li className='nav-item' style={{float: 'none'}}>
                <a className={'nav-link ' + (active ? 'active' : '') } id={tabkey + '-tab'} data-toggle='pill' href={'#pills-' + tabkey} role='tab' aria-controls={'pills-' + tabkey} aria-selected='true' style={{letterSpacing: letterSpacing}}>{title}</a>
            </li>
        )
    }
}

export class TabContent extends Component {
    render() {
        const { active, tabkey, title, children } = this.props

        return (
            <div className={'entry-content tab-pane ' + (active ? 'show active' : '')} id={'pills-'+tabkey} role='tabpanel' aria-labelledby={'pills-'+tabkey+'-tab'}>
                <h2>{title}</h2>
                {children}
            </div>
        )
    }
}
