@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Socialmedia Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add"
                                onclick="resetFormAdd();">Add New <i class="fa fa-plus"></i></a>
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Info
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Modal Add !-->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Socialmedia</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_add' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_add" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_add" name="name_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Link <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="link_add" name="link_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-8">
                                <textarea id="description_add" name="description_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update!-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Socialmedia</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_edit_old" name="name_edit_old" type="hidden" />
                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Link <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="link_edit_old" name="link_edit_old" type="hidden" />
                                <input id="link_edit" name="link_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-8">
                                <input id="description_edit_old" name="description_edit_old" type="hidden" />
                                <textarea id="description_edit" name="description_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden" />
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Add Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Confirmation!-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden" />
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type': 'GET',
                'url': 'socialmedia/list'
            },
            "columnDefs": [{
                "targets": 1,
                "className": "dt-center",
                "orderable": false
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth": "90%"}, {"sWidth": "10%"}]
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function resetFormAdd() {
        validator_add.resetForm();

        $('#modal_add #name_add').val('');
        $('#modal_add #link_add').val('');
        $('#modal_add #description_add').val('');
    }

    function submitForm(action) {
        if (action == 'create') {
            $name = $('#modal_add #name_add').val();
            $link = $('#modal_add #link_add').val();
            $description = $('#modal_add #description_add').val();

            $(".loading").show();

            var form = $('#form_add')[0];
            var fdata = new FormData(form);

            fdata.append("name", $name);
            fdata.append("link", $link);
            fdata.append("description", $description);

            $.ajax({
                type: 'POST',
                url: "socialmedia/store",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fdata, 
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status == 'success') {
                        $('#confirm_modal_add').modal('toggle');
                        $('#modal_add').modal('toggle');

                        resetFormAdd();

                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function () {
                            $("#submit_alert_success").slideUp();
                        }, 6000);
                    } else {
                        $('#confirm_modal_add').modal('toggle');

                        $("#modal_message_add").text(response.message);
                        $("#modal_message_add").show();
                        setTimeout(function() { $("#modal_message_add").slideUp(); }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if (action == 'update') {
            $id = $('#update_id').val();
            $name = $('#modal_edit #name_edit').val();
            $link = $('#modal_edit #link_edit').val();
            $description = $('#modal_edit #description_edit').val();

            $name_old = $('#modal_edit #name_edit_old').val();
            $link_old = $('#modal_edit #link_edit_old').val();
            $description_old = $('#modal_edit #description_edit_old').val();

            if ($name == $name_old && $link == $link_old && $description == $description_old) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $(".loading").show();

                var form = $('#form_edit')[0];
                var fdata = new FormData(form);

                fdata.append("id", $id);
                fdata.append("name", $name);
                fdata.append("link", $link);
                fdata.append("description", $description);

                $.ajax({
                    type: 'POST',
                    url: "socialmedia/update",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fdata, 
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == 'success') {
                            $('#confirm_modal_edit').modal('toggle');
                            $('#modal_edit').modal('toggle');

                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function () {
                                $("#submit_alert_success").slideUp();
                            }, 6000);
                        } else {
                            $('#confirm_modal_edit').modal('toggle');

                            $("#modal_message_edit").html(response.message);
                            $("#modal_message_edit").show();
                            setTimeout(function () {
                                $("#modal_message_edit").slideUp();
                            }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');
        $(".loading").show();
        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "socialmedia/destroy",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            },
            success: function (response) {
                if (response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function () {
                        $("#submit_alert_success").slideUp();
                    }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function () {
                        $("#submit_alert_failed").slideUp();
                    }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "socialmedia/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#update_id').val(response.id);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #link_edit').val(response.link);
                $('#modal_edit #description_edit').val(response.description);

                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #link_edit_old').val(response.link);
                $('#modal_edit #description_edit_old').val(response.description);

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Add Validation
    $().ready(function() {
        validator_add = $("#form_add").validate({
            rules: {
                name_add: {
                    required: true,
                    minlength: 2
                },
                link_add: {
                    required: true,
                    url: true
                }
            },
            descriptions: {
                name_add: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                link_add: {
                    required: "Please enter an link",
                    url: "Please enter a valid url"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    //Edit Validation
    $().ready(function() {
        validator_edit = $("#form_edit").validate({
            rules: {
                name_edit: {
                    required: true,
                    minlength: 2
                },
                link_edit: {
                    required: true,
                    url: true
                }
            },
            descriptions: {
                name_edit: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                link_edit: {
                    required: "Please enter an link",
                    url: "Please enter a valid url"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });

</script>
@endsection
