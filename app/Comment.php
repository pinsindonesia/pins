<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'comment', 'article_id', 'is_featured'
    ];

    protected $dates = ['deleted_at'];

    public function article() {
        return $this->belongsTo('App\Article', 'article_id');
    }

    public function scopeShowed($query) {
        return $query
            ->where('showed', 'true');
    }
}
