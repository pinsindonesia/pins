<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Enquiry;

class EnquiryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
	}
	
    public function index()
    {
        return view('pages/enquiry');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 3;}; 
		$order_fields = array('name', '', '', 'created_at');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = Enquiry::where('name', 'LIKE', '%'.$filter.'%')
    							->orWhere('email', 'LIKE', '%'.$filter.'%')->get();
    	$list_filtered = Enquiry::where('name', 'LIKE', '%'.$filter.'%')
    							->orWhere('email', 'LIKE', '%'.$filter.'%')
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
			$action = '
						<center>
							<button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete"><i class="fa fa-trash"></i></button>
						</center>
					';

		    array_push($data, 
				array(
					'<b>Name</b> ' . $value['name'] . '<br/>' . '<b>Email</b> '. $value['email'] .'<br/>'. '<b>Phone</b> '. $value['phone'],
                    $value['message'],
					$action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
	}
	
	public function store(Request $request) {
    	//rules
        $rules=array(
        	'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required'
        );
          
        //message error 
        $messages=array(
        	'name.required' => 'Name required.',
            'email.required' => 'Email required.',
            'phone.required' => 'Phone required.',
            'message.required' => 'Message required.'
        );

		$validator = Validator::make($request->all(),$rules,$messages);
		
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			$table = new Enquiry;
            $table->name = $request->name;
			$table->email = $request->email;
			$table->phone = $request->phone;
			$table->message = $request->message;

            if($table->save()) {
	        	return response()->json([
		            'status' => 'success',
		            'message' => 'Form Submited.'
		        ]);
	        } else {
	        	return response()->json([
                    'status' => 'error',
                    'message' => 'Submit Failed.'
                ]);
	        }
        }
	}

	public function destroy(Request $request) {
    	$deleted = Enquiry::where('id', $request->id)->delete();

        if($deleted) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Record Deleted.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
		$record = Enquiry::where('id', $request->id)->first();
    	return response()->json($record);
    }
	
	public function update(Request $request) {
    	$updated = 0;

    	//rules
        $rules=array(
        	'id' => 'required',
        	'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required'
        );
          
        //message error 
        $messages=array(
        	'id.required' => 'ID required.',
        	'name.required' => 'Name required.',
            'email.required' => 'Email required.',
            'phone.required' => 'Phone required.',
            'message.required' => 'Message required.'
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			//check email existing
	    	$exist = Enquiry::where('name', $request->name)->first();

	    	if($exist) {
	    		if($exist['name'] == $request->name_old) {
	    			$updated = Enquiry::where('id', $request->id)
		                              ->update([
		                                    'name' => $request->name,
		                                    'email' => $request->email,
		                                    'phone' => $request->phone,
		                                    'message' => $request->message
		                                ]);
	    		} else {
	    			return response()->json([
		                'status' => 'error',
		                'message' => 'The email has already been taken.'
		            ]);
	    		}
	    	} else {
	    		$updated = Enquiry::where('id', $request->id)
		                            ->update([
		                                    'name' => $request->name,
		                                    'email' => $request->email,
		                                    'phone' => $request->phone,
		                                    'message' => $request->message
		                                ]);
	    	}
        }

        if($updated) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Form Updated.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
