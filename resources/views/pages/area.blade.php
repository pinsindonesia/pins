@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Area Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add"
                                onclick="resetFormAdd();">Add New <i class="fa fa-plus"></i></a>
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Info
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Modal Add !-->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Area</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_add" name="name_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Location <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="location_add" name="location_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-8">
                                <input id="email_add" name="email_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telp</label>
                            <div class="col-md-8">
                                <input id="telp_add" name="telp_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-3">Fax</label>
                            <div class="col-md-8">
                                <input id="fax_add" name="fax_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-2">
                                <input type="checkbox" id="featured_add" name="featured_add"/> Featured
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update!-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Area</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_edit_old" name="name_edit_old" type="hidden" />
                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Location <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="location_edit_old" name="location_edit_old" type="hidden" />
                                <input id="location_edit" name="location_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-8">
                                <input id="email_edit_old" name="email_edit_old" type="hidden" />
                                <input id="email_edit" name="email_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telp</label>
                            <div class="col-md-8">
                                <input id="telp_edit_old" name="telp_edit_old" type="hidden" />
                                <input id="telp_edit" name="telp_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-3">Fax</label>
                            <div class="col-md-8">
                                <input id="fax_edit_old" name="fax_edit_old" type="hidden" />
                                <input id="fax_edit" name="fax_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-2">
                                <input id="featured_edit_old" name="featured_edit_old" type="hidden"/>
                                <input type="checkbox" id="featured_edit" name="featured_edit"/> Featured
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden" />
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Detail!-->
<div class="modal fade" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detail Area</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Name</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="name_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Location</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="location_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Add Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden" />
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type': 'GET',
                'url': 'area/list'
            },
            "columnDefs": [{
                "targets": 1,
                "orderable": false,
                "className": "dt-center"
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{
                "sWidth": "90%"
            }, {
                "sWidth": "10%"
            }]
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function resetFormAdd() {
        validator_add.resetForm();

        $('#modal_add #name_add').val('');
        $('#modal_add #location_add').val('');
        $('#modal_add #email_add').val('');
        $('#modal_add #telp_add').val('');
        $('#modal_add #fax_add').val('');
        $('#modal_add #featured_add').prop('checked', false);
    }

    function submitForm(action) {
        if (action == 'create') {
            $name = $('#modal_add #name_add').val();
            $location = $('#modal_add #location_add').val();
            $email = $('#modal_add #email_add').val();
            $telp = $('#modal_add #telp_add').val();
            $fax = $('#modal_add #fax_add').val();
            $featured = $('#modal_add #featured_add').prop('checked');

            $('#confirm_modal_add').modal('toggle');
            $('#modal_add').modal('toggle');

            resetFormAdd();
            $(".loading").show();

            $.ajax({
                type: 'POST',
                url: "area/store",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'name': $name,
                    'location': $location,
                    'email': $email,
                    'telp': $telp,
                    'fax': $fax,
                    'featured' : $featured
                },
                success: function (response) {
                    if (response.status == 'success') {
                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function () {
                            $("#submit_alert_success").slideUp();
                        }, 6000);
                    } else {
                        table.ajax.reload();
                        $("#submit_alert_failed").html(response.message);
                        $("#submit_alert_failed").show();
                        setTimeout(function () {
                            $("#submit_alert_failed").slideUp();
                        }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if (action == 'update') {
            $id = $('#update_id').val();
            $name = $('#modal_edit #name_edit').val();
            $location = $('#modal_edit #location_edit').val();
            $email = $('#modal_edit #email_edit').val();
            $telp = $('#modal_edit #telp_edit').val();
            $fax = $('#modal_edit #fax_edit').val();
            $featured = $('#modal_edit #featured_edit').prop('checked');

            $name_old = $('#modal_edit #name_edit_old').val();
            $location_old = $('#modal_edit #location_edit_old').val();
            $email_old = $('#modal_edit #email_edit_old').val();
            $telp_old = $('#modal_edit #telp_edit_old').val();
            $fax_old = $('#modal_edit #fax_edit_old').val();
            $featured_old = $('#modal_edit #featured_edit_old').val() == "true" ? true : false;

            if ($name == $name_old && $location == $location_old && $email == $email_old && $telp == $telp_old && $fax == $fax_old && $featured == $featured_old) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');

                $(".loading").show();

                $.ajax({
                    type: 'PUT',
                    url: "area/update",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'id': $id,
                        'name': $name,
                        'name_old': $name_old,
                        'location': $location,
                        'email': $email,
                        'telp': $telp,
                        'fax': $fax,
                        'featured' : $featured
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function () {
                                $("#submit_alert_success").slideUp();
                            }, 6000);
                        } else {
                            table.ajax.reload();
                            $("#submit_alert_failed").html(response.message);
                            $("#submit_alert_failed").show();
                            setTimeout(function () {
                                $("#submit_alert_failed").slideUp();
                            }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');
        $(".loading").show();
        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "area/destroy",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            },
            success: function (response) {
                if (response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function () {
                        $("#submit_alert_success").slideUp();
                    }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function () {
                        $("#submit_alert_failed").slideUp();
                    }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "area/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#update_id').val(response.id);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #location_edit').val(response.location);
                $('#modal_edit #email_edit').val(response.email);
                $('#modal_edit #telp_edit').val(response.telp);
                $('#modal_edit #fax_edit').val(response.fax);

                response.featured == "true" ? $('#featured_edit').prop('checked', true) : $('#featured_edit').prop('checked', false);

                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #location_edit_old').val(response.location);
                $('#modal_edit #email_edit_old').val(response.email);
                $('#modal_edit #telp_edit_old').val(response.telp);
                $('#modal_edit #fax_edit_old').val(response.fax);
                $('#modal_edit #featured_edit_old').val(response.featured);

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Add Validation
    $().ready(function () {
        validator_add = $("#form_add").validate({
            rules: {
                name_add: {
                    required: true,
                    minlength: 2
                },
                location_add: {
                    required: true,
                    minlength: true
                }
            },
            messages: {
                name_add: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                location_add: {
                    required: "Please enter an location",
                    location: "Please enter a valid location"
                }
            },
            submitHandler: function (form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    //Edit Validation
    $().ready(function () {
        validator_edit = $("#form_edit").validate({
            rules: {
                name_edit: {
                    required: true,
                    minlength: 2
                },
                location_edit: {
                    required: true,
                    minlength: 2
                },
            },
            messages: {
                name_edit: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                location_edit: {
                    required: "Please enter an location",
                    minlength: "Your location must consist of at least 2 characters"
                }
            },
            submitHandler: function (form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });

</script>
@endsection
