import React, { Component } from 'react'
import { connect } from 'react-redux'

import SectionHeadline from './SectionHeadline'

import { addEnquiry, resetEnquiry } from '../redux/actions/enquiryActions';
import { Helpers } from '../utils';

@connect((store) => {
    return {
        enquiry: store.enquiry.enquiry,
        eAdded: store.enquiry.added,
        processing: store.enquiry.adding,
    }
})
export default class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            phone: '',
            message: '',
        };

        this.nameHandleChange = this.nameHandleChange.bind(this)
        this.emailHandleChange = this.emailHandleChange.bind(this)
        this.phoneHandleChange = this.phoneHandleChange.bind(this)
        this.messageHandleChange = this.messageHandleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        this.props.dispatch(resetEnquiry())
    }

    nameHandleChange(event) {
        this.setState({name: event.target.value})
        this.validate()
    }

    emailHandleChange(event) {
        this.setState({email: event.target.value})
        this.validate()
    }

    phoneHandleChange(event) {
        this.setState({phone: event.target.value})
        this.validate()
    }

    messageHandleChange(event) {
        this.setState({message: event.target.value})
        this.validate()
    }

    validate() {
        const {name, phone, email, message} = this.state
        if (!Helpers.isEmptyString(name) && !Helpers.isEmptyString(email) && !Helpers.isEmptyString(phone) && !Helpers.isEmptyString(message)) {
            $('#submit').prop('disabled', false)
        } else {
            $('#submit').prop('disabled', true)
        }
    }

    handleSubmit(event) {
        this.props.dispatch(
            addEnquiry(this.state.name,
                this.state.phone,
                this.state.email,
                this.state.message))

        this.setState({
            name: '',
            phone: '',
            email: '',
            message: ''
        })

        event.preventDefault()
    }

    render() {
        const { title, phone, mail, address, eAdded, processing } = this.props

        let comp = ''

        if (eAdded) comp = <div id='sendmessage' className='show'>{Helpers.localeText('message_sent')}</div>
        else if (processing) comp = <div id='sendmessage' className='show'>{Helpers.localeText('loading')}</div>

        return (
                <div>
                    <SectionHeadline title={title} />

                    <div className='row'>
                        <div className='col-md-4 col-sm-4 col-xs-12'>
                            <div className='contact-icon text-center'>
                            <div className='single-icon'>
                                <i className='fa fa-mobile'></i>
                                <p>{phone}</p>
                            </div>
                            </div>
                        </div>

                        <div className='col-md-4 col-sm-4 col-xs-12'>
                            <div className='contact-icon text-center'>
                            <div className='single-icon'>
                                <i className='fa fa-envelope-o'></i>
                                <p>{mail}</p>
                            </div>
                            </div>
                        </div>

                        <div className='col-md-4 col-sm-4 col-xs-12'>
                            <div className='contact-icon text-center'>
                            <div className='single-icon'>
                                <i className='fa fa-map-marker'></i>
                                <p>{address}</p>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div className='row'>

                    {/* <div className='col-md-6 col-sm-6 col-xs-12'>
                        <div id='google-map' data-latitude='40.713732' data-longitude='-74.0092704'></div>
                    </div> */}

                    <div className='col-md-12 col-sm-12 col-xs-12'>
                        <div className='form contact-form'>
                        {comp}
                        <div id='errormessage'></div>
                        <form onSubmit={this.handleSubmit}>
                            <div className='form-group'>
                            <input type='text' name='name' className='form-control' id='name' placeholder={Helpers.localeText('name')} data-rule='minlen:4' data-msg='Please enter at least 4 chars' value={this.state.name} onChange={this.nameHandleChange}/>
                            <div className='validation'></div>
                            </div>
                            <div className='form-group'>
                            <input type='email' className='form-control' name='email' id='email' placeholder={Helpers.localeText('email')} data-rule='email' data-msg='Please enter a valid email' value={this.state.email} onChange={this.emailHandleChange}/>
                            <div className='validation'></div>
                            </div>
                            <div className='form-group'>
                            <input type='text' className='form-control' name='subject' id='subject' placeholder={Helpers.localeText('phone')} data-rule='minlen:4' data-msg='Please enter your phone' value={this.state.phone} onChange={this.phoneHandleChange}/>
                            <div className='validation'></div>
                            </div>
                            <div className='form-group'>
                            <textarea className='form-control' name='message' rows='5' data-rule='required' data-msg='Please write something for us' placeholder={Helpers.localeText('message')} value={this.state.message} onChange={this.messageHandleChange} />
                            <div className='validation'></div>
                            </div>
                            <div className='text-center'><button id='submit' type='submit' disabled>{Helpers.localeText('send_message')}</button></div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
