import React, { Component } from 'react'
import { connect } from 'react-redux'

import AllNews from '../components/AllNews'
import Slider from '../components/Slider'

import { setActiveMenu } from '../redux/actions/menuActions'
import { fetchNewsSlider } from '../redux/actions/sliderActions';

@connect((store) => {
    return {
        sliders: store.slider.sliders
    }
})
export default class NewsFull extends Component {
    constructor() {
        super()
    }

    componentWillMount() {
        this.props.dispatch(fetchNewsSlider())
    }

    componentDidMount() {
        this.props.dispatch(setActiveMenu(this.props.href))
    }

    render() {
        return (
            <div>
                <Slider sliders={this.props.sliders} />
                <AllNews />
            </div>
        )
    }
}
