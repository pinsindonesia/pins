@extends('layouts.app')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Partners Table</h3>
        </div>
        <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
        <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add" onclick="resetFormAdd();">Add New <i class="fa fa-plus"></i></a>
                    <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
            <br>
            <table class="table table-striped table-bordered table-hover" id="table_list">
                <thead>
                    <tr>
                        <th>
                             Info
                        </th>
                        <th>
                             Logo
                        </th>
                        <th>
                             Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</section>


<!-- create-->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Partner</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_add' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_add" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Product <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" id="product_add" name="product_add" data-live-search="true" title="-- choose product --" multiple>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_add" name="name_add" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Image <span class="required">
                            * </span></label>
                            <div class="col-md-8">
                                <img id="uploadPreview_add" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_add" name="image_add" type="file" data-required="1" class="form-control" onchange="PreviewImage_add();"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Description <span class="required">
                            * </span></label>
                            <div class="col-md-8">
                                <textarea name="description_add" id="description_add" cols="" rows="5" data-required="1" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
    
        </div>
        
    </div>
    
</div>

<!-- update-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Partner</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_edit' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_edit" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Product <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="product_edit_old" name="product_edit_old" type="hidden"/>
                                <select class="form-control" id="product_edit" name="product_edit" data-live-search="true" title="-- choose product --" multiple>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="required">
                            * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="name_edit_old" name="name_edit_old" type="hidden"/>
                                <input id="name_edit" name="name_edit" type="text" data-required="1" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Image <span class="required">
                            * </span></label>
                            <div class="col-md-8">
                                <img id="uploadPreview_edit" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_edit" name="image_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_edit();"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Description <span class="required">
                            * </span></label>
                            <div class="col-md-8">
                                <textarea id="description_edit_old" name="description_edit_old" style="display: none;"></textarea>
                                <textarea name="description_edit" id="description_edit" cols="" rows="5" data-required="1" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden"/>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- update-->
<div class="modal fade" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detail Partner</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Product</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="product_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Name</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="name_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Image</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <img id="image_detail" alt="Image" style="width: 100px; height: 100px;" />
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-4 col-md-4">
                        <label class="label-control"><strong>Description</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="description_detail" class="label-control"></label>
                    </div>
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- submit confirmation-->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- update confirmation -->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- delete confirmation -->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                 Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden"/>
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Delete</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function() {       
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type' : 'GET',
                'url': 'partner/list'     
            },
            "columnDefs": [{
                "targets": 2,
                "className": "dt-center"
            }, {
                "targets": [1,2],
                "orderable": false
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth":"70%"},{"sWidth":"21%"},{"sWidth":"9%"}]
        });

        //product list in form
        $.ajax({
            type: 'GET',
            url: "partner/product_list", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                $("#modal_add #product_add").html("");
                $("#modal_edit #product_edit").html("");
                for(var item of response.data) {
                    $("#modal_add #product_add").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    $("#modal_edit #product_edit").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                }

                $('#product_add').selectpicker();
                $('#product_edit').selectpicker();
            }
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function resetFormAdd() {
        validator_add.resetForm();  

        $('#modal_add #product_add').selectpicker('val', '');;
        $('#modal_add #name_add').val('');
        $('#modal_add #image_add').val('');
        $('#modal_add #uploadPreview_add').attr("src", "");
        $('#modal_add #description_add').val('');
    }

    function PreviewImage_add() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_add").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_add").src = oFREvent.target.result;
        };
    };

    function PreviewImage_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_edit").src = oFREvent.target.result;
        };
    };

    //create
    function submitForm(action) {
        if(action == 'create') {
            $product          = $('#modal_add #product_add').val();
            $name             = $('#modal_add #name_add').val();
            $image            = $('#modal_add #image_add');
            $description      = $('#modal_add #description_add').val();

            $(".loading").show();

            var form = $('#form_add')[0];
            var fdata = new FormData(form);

            fdata.append("product", $product);
            fdata.append("name", $name);
            fdata.append("image", $image[0].files[0]);
            fdata.append("description", $description);

            $.ajax({
                type: 'POST',
                url: "partner/store", 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fdata, 
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.status == 'success') {
                        $('#confirm_modal_add').modal('toggle');
                        $('#modal_add').modal('toggle');

                        resetFormAdd();

                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                    } else {
                        $('#confirm_modal_add').modal('toggle');

                        $("#modal_message_add").text(response.message);
                        $("#modal_message_add").show();
                        setTimeout(function() { $("#modal_message_add").slideUp(); }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if(action == 'update') {
            $id                 = $('#update_id').val();
            $product            = $('#modal_edit #product_edit').val();
            $name               = $('#modal_edit #name_edit').val();
            $image              = $('#modal_edit #image_edit');
            $description        = $('#modal_edit #description_edit').val();

            $product_old        = $('#modal_edit #product_edit_old').val();
            $name_old           = $('#modal_edit #name_edit_old').val();
            $description_old    = $('#modal_edit #description_edit_old').val();

            if($product.join(",") == $product_old && $name == $name_old && $description == $description_old && $image[0].files.length == 0) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $(".loading").show();

                var form = $('#form_edit')[0];
                var fdata = new FormData(form);

                fdata.append("id", $id);
                fdata.append("product", $product);
                fdata.append("name", $name);
                fdata.append("image", $image[0].files[0]);
                fdata.append("description", $description);  

                $.ajax({
                    type: 'POST',
                    url: "partner/update", 
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fdata, 
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response.status == 'success') {
                            $('#confirm_modal_edit').modal('toggle');
                            $('#modal_edit').modal('toggle');

                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                        } else {
                            $('#confirm_modal_edit').modal('toggle');

                            $("#modal_message_edit").html(response.message);
                            $("#modal_message_edit").show();
                            setTimeout(function() { $("#modal_message_edit").slideUp(); }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    //delete
    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);   
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');

        $(".loading").show();

        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "partner/destroy", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            }, 
            success: function(response){
                if(response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function() { $("#submit_alert_failed").slideUp(); }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    //update
    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "partner/detail", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            }, 
            success: function(response){
                validator_edit.resetForm();

                var product_array = response.product_id.split(',');

                $('#update_id').val(response.id);
                $('#modal_edit #product_edit').selectpicker('val', product_array);
                $('#modal_edit #name_edit').val(response.name);
                $('#modal_edit #uploadPreview_edit').attr("src", "{{asset('storage/partner/image')}}".replace("image", response.image));
                $('#modal_edit #description_edit').val(response.description);

                $('#modal_edit #product_edit_old').val(response.product_id);
                $('#modal_edit #name_edit_old').val(response.name);
                $('#modal_edit #description_edit_old').val(response.description);
            
                $('#modal_edit').modal('toggle');  

                $(".loading").hide();
            }
        });  
    }

    //detail
    function _detail(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "partner/detail", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            }, 
            success: function(response){
                $('#modal_detail #product_detail').text(response.product.title);
                $('#modal_detail #name_detail').text(response.name);
                $('#modal_detail #image_detail').attr("src", "{{asset('storage/partner/image')}}".replace("image", response.image));
                $('#modal_detail #description_detail').text(response.description);

                $('#modal_detail').modal('toggle');  

                $(".loading").hide();
            }
        });  
    }

    //form validation

    $().ready(function() {
        // validate signup form on keyup and submit
        validator_add = $("#form_add").validate({
            rules: {
                product_add: {
                    required: true
                },
                name_add: {
                    required: true
                },
                image_add: {
                    required: true
                },
                description_add: {
                    required: true
                }
            },
            messages: {
                product_add: {
                    required: "Please choose a product"
                },
                name_add: {
                    required: "Please enter a name"
                },
                image_add: {
                    required: "Please choose an image"
                },
                description_add: {
                    required: "Please enter a description"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    $().ready(function() {
        // validate signup form on keyup and submit
        validator_edit = $("#form_edit").validate({
            rules: {
                product_edit: {
                    required: true
                },
                name_edit: {
                    required: true
                },
                description_edit: {
                    required: true
                }
            },
            messages: {
                product_edit: {
                    required: "Please choose a product"
                },
                name_edit: {
                    required: "Please enter a name"
                },
                description_edit: {
                    required: "Please enter a description"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });
</script>
@endsection