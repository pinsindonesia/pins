<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/user');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', 'email', '');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = User::where('name', 'LIKE', '%'.$filter.'%')->get();
    	$list_filtered = User::where('name', 'LIKE', '%'.$filter.'%')
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
            $usersession = $request->session()->get('userid');
            $disabled = $value['id'] == $usersession ? 'disabled' : '';

			$action = '
					  <center>
                        <button class="btn btn-sm btn-warning" onclick="_edit(\''.$value['id'].'\');" title="Edit"><i class="fa fa-edit"></i></button>
	                    <button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete" '.$disabled.'><i class="fa fa-trash"></i></button>
					  </center>
					';

		    array_push($data, 
				array(
					$value['name'],
					$value['email'],
					$action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
    }

    public function store(Request $request) {
    	//check email existing
    	$exist = User::where('email', $request->email)->first();

    	if($exist) {
    		return response()->json([
                'status' => 'error',
                'message' => 'The email has already been taken.'
            ]);
    	}

    	//rules
        $rules=array(
        	'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        );
          
        //message error 
        $messages=array(
        	//'name.required' => 'Name required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			$table = new User;

            $table->name = $request->name;
            $table->email = $request->email;
            $table->password = Hash::make($request->password);

            if($table->save()) {
	        	return response()->json([
		            'status' => 'success',
		            'message' => 'Form Submited.'
		        ]);
	        } else {
	        	return response()->json([
                    'status' => 'error',
                    'message' => 'Submit Failed.'
                ]);
	        }
        }
    }

    public function destroy(Request $request) {
    	$deleted = User::where('id', $request->id)->delete();

        if($deleted) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Record Deleted.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
		$record = User::where('id', $request->id)->first();
    	
    	return response()->json($record);
    }

    public function update(Request $request) {
    	$updated = 0;

    	//rules
        $rules=array(
        	'id' => 'required',
        	'name' => 'required',
            'email' => 'required'
        );
          
        //message error 
        $messages=array(
        	'id.required' => 'ID required.',
        	'name.required' => 'Name required.',
            'email.required' => 'Email required.'
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			//check email existing
	    	$exist = User::where('email', $request->email)->first();

	    	if($exist) {
	    		if($exist['email'] == $request->email_old) {
	    			$updated = User::where('id', $request->id)
		                              ->update([
		                                    'name' => $request->name,
		                                    'email' => $request->email
		                                ]);
	    		} else {
	    			return response()->json([
		                'status' => 'error',
		                'message' => 'The email has already been taken.'
		            ]);
	    		}
	    	} else {
	    		$updated = User::where('id', $request->id)
	                              ->update([
	                                    'name' => $request->name,
	                                    'email' => $request->email
	                                ]);
	    	}
        }

        if($updated) {
            return response()->json([
	            'status' => 'success',
	            'message' => 'Form Updated.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }

    public function change_password(Request $request) {
        $updated = 0;

        //rules
        $rules=array(
            'id' => 'required',
            'password_new' => 'required',
            'password_old' => 'required'
        );
          
        //message error 
        $messages=array(
            //'name.required' => 'Name required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
            //check user credential
            $user_password = User::find($request->id)->password;

            if(Hash::check($request->password_old, $user_password)) {
                $updated = User::where('id', $request->id)
                                  ->update([
                                        'password' => Hash::make($request->password_new)
                                    ]);

                if($updated) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Password Updated.'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Update Password Failed.'
                    ]);
                }   
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Your Password is wrong.'
                ]);
            }
        }
    }
}
