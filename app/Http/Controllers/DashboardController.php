<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Analytic;
use App\Article;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/dashboard');
    }

    public function article_list(Request $request)
    {
        $list = Article::all();

        $data = array();
        foreach ( $list as $value ) {
            array_push($data,
                array(
                    $value['id'],
                    $value['title_id'] . " / " . $value['title_en']
                )
            );
        }

        $result["data"] = $data;

        return response()->json($result);
    }

    public function visitor(Request $request) {
    	$days = 7; //last 7 days
    	$date = new Carbon;
    	$curr_date = $date->now();
    	$past_date = $date->subDays($days);

		$list = Analytic::where('created_at', '>=', $past_date)
		                   ->where('created_at', '<=', $curr_date)
		                   ->where('url', 'LIKE', '%nav/home%')
		                   ->get();

    	$datax = array();
    	$datay = array();
    	$record_count = 0;
    	for($i = 1; $i <= $days; $i++) {
    		$each_past_date = $past_date->addDay()->format('M d');

			foreach ( $list as $key=>$value ) {
                $record_past_date = $value['created_at']->format('M d');

                $nowUG = $value['useragent'];
                $cond = true;
                if ($key + 1 < count($list)) {
                    $nextUG = $list[$key + 1]['useragent'];
                    if ($nowUG == $nextUG) $cond = false;
                }

				if($each_past_date === $record_past_date && $cond) { $record_count++; }
			}

			array_push($datax, $past_date->format('M d') );

			array_push($datay, $record_count);

			$record_count = 0;
    	}

		$result["valuex"] = $datax;
		$result["valuey"] = $datay;

		return response()->json($result);
	}

	public function article(Request $request) {
    	$days = 7; //last 7 days
    	$date = new Carbon;
    	$curr_date = $date->now();
    	$past_date = $date->subDays($days);

		$list = Analytic::where('created_at', '>=', $past_date)
		                   ->where('created_at', '<=', $curr_date)
		                   ->where('url', 'LIKE', '%/news/'.$request->id.'%')
		                   ->get();

    	$datax = array();
    	$datay = array();
    	$record_count = 0;
    	for($i = 1; $i <= $days; $i++) {
    		$each_past_date = $past_date->addDay()->format('M d');

			foreach ( $list as $key=>$value ) {
                $record_past_date = $value['created_at']->format('M d');

                $nowUG = $value['useragent'];
                $cond = true;
                if ($key + 1 < count($list)) {
                    $nextUG = $list[$key + 1]['useragent'];
                    if ($nowUG == $nextUG) $cond = false;
                }

				if($each_past_date === $record_past_date && $cond) { $record_count++; }
			}

			array_push($datax, $past_date->format('M d') );

			array_push($datay, $record_count);

			$record_count = 0;
    	}

		$result["valuex"] = $datax;
		$result["valuey"] = $datay;

		return response()->json($result);
	}
}
