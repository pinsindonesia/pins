import React, { Component } from 'react'
import { connect } from 'react-redux'

import NewsItem from './NewsItem'
import SectionHeadline from './SectionHeadline'
import { fetchNewsAll } from '../redux/actions/newsActions';

@connect((store) => {
    return {
        news: store.news.news,
        fetched: store.news.fetched
    }
})
export default class LatestNews extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.dispatch(fetchNewsAll())
    }

    render() {
        let newslist = []

        const { news } = this.props
        const diff = Math.ceil(news.length/3);
        if (diff > 0) {
            for (let i = 0; i < diff; i++) {
                let nitems = []
                for (let j = 0; j < 3; j++) {
                    const nws = news[(i*3) + j];
                    if (nws) {
                        nitems.push(<NewsItem
                            key={j}
                            wowDur='1s'
                            wowDelay={((j % 3)*0.3)+'s'}
                            href={'/#/news/'+nws.id}
                            img={nws.img}
                            comment={nws.comments}
                            createdAt={nws.date}
                            title={nws.title}
                            content={nws.content}
                        />)
                    }
                }
                newslist.push(<div className='row' key={i}>{nitems}</div>)
            }
        }

        return (
            <div id='blog' className='blog-area'>
                <div className='blog-inner area-padding'>
                <div className='blog-overly'></div>
                <div className='container '>
                    <SectionHeadline title='All News' />

                    {newslist.map((v, i) => {
                        return v
                    })}
                </div>
                </div>
            </div>
        );
    }
}
