export default function reducer(state={
    products: {},
    fetching: false,
    fetched: false,
    error: null
}, action) {
    switch (action.type) {
        case 'FETCH_PRODUCT': {
            return {...state, fetching: true}
        }
        case 'FETCH_PRODUCT_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }
        case 'FETCH_PRODUCT_FULFILLED': {
            return {...state, fetching: false, fetched: true, products: action.payload}
        }
    }

    return state
}
