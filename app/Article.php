<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'description_short', 'description_long', 'image', 'video', 'user_id', 'is_featured'
    ];

    protected $dates = ['deleted_at'];

    public function author() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function scopeFeatured($query) {
        return $query
            ->where('is_featured', 'true')
            ->latest();
    }
}
