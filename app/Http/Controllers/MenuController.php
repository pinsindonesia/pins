<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Menu;
use App\Content;
use App\Product;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	}
	
    public function index()
    {
        return view('pages/menu');
    }

    public function list(Request $request) {
		$list = Menu::orderBy("order", "ASC")->get();

    	$data = array();			
		foreach ( $list as $value1 ) {
            //get parent
            if($value1['parent_id'] == null) {
                $child = array();
                foreach ( $list as $value2 ) {
                    //get child
                    if($value2['parent_id'] == $value1['id']) {
                        $gchild = array();
                        foreach ( $list as $value3 ) {
                            //get grandchild
                            if($value3['parent_id'] == $value2['id']) {
                                array_push($gchild, 
                                    array(
                                        "id"        => $value3['id'],
                                        "_type"     => $value3['type'],
                                        "_self"     => $value3['id'],
                                        "text"      => $value3['name_id'] . ' / ' . $value3['name_en'],
                                        "color"     => "black",
                                        "backColor" => "#FFFFFF"
                                    )
                                );
                            }
                        }

                        if(count($gchild) == 0) {
                            array_push($child, 
                                array(
                                    "id"        => $value2['id'],
                                    "_type"     => $value2['type'],
                                    "_self"     => $value2['id'],
                                    "text"      => $value2['name_id'] . ' / ' . $value2['name_en'],
                                    "color"     => "black",
                                    "backColor" => "#F5F5F5"
                                )
                            );
                        } else {
                            array_push($child, 
                                array(
                                    "id"        => $value2['id'],
                                    "_type"     => $value2['type'],
                                    "_self"     => $value2['id'],
                                    "text"      => $value2['name_id'] . ' / ' . $value2['name_en'],
                                    "color"     => "black",
                                    "backColor" => "#F5F5F5",
                                    "nodes"     => $gchild
                                )
                            );
                        }
                    }
                }

                if(count($child) == 0) {
                    array_push($data, 
                        array(
                            "id"        => $value1['id'],
                            "_type"     => $value1['type'],
                            "_self"     => $value1['id'],
                            "text"      => $value1['name_id'] . ' / ' . $value1['name_en'],
                            "color"     => "black",
                            "backColor" => "#E8E8E8"
                        )
                    );
                } else {
                    array_push($data, 
                        array(
                            "id"        => $value1['id'],
                            "_type"     => $value1['type'],
                            "_self"     => $value1['id'],
                            "text"      => $value1['name_id'] . ' / ' . $value1['name_en'],
                            "color"     => "black",
                            "backColor" => "#E8E8E8",
                            "nodes"     => $child
                        )
                    );
                }
            }
		}
		
		$result["data"] = $data;

		return response()->json($result);
	}

    public function parent_list(Request $request) {
        $root = Menu::select("id")->where("parent_id", null)->get(); 

        $list = Menu::whereIn("parent_id", $root)->orWhere("parent_id", "=" ,null)->orderBy("order", "ASC")->get();
        
        $data = array();            
        foreach ( $list as $value ) {
            array_push($data, 
                array(
                    $value['id'],
                    $value['name_id'] . " / " . $value['name_en']
                )
            );
        }
        
        $result["data"] = $data;

        return response()->json($result);
    }

    public function content_list(Request $request) {
        $data = array();  
        if($request->type == 0) {
            $list = Content::where("type", $request->type)->get();

            foreach ( $list as $value ) {
                array_push($data, 
                    array(
                        $value['id'],
                        $value['name']
                    )
                );
            }
        } else if($request->type == 2) {
            $list = Product::all();

            foreach ( $list as $value ) {
                array_push($data, 
                    array(
                        $value['id'],
                        $value['title']
                    )
                );
            }
        }
        
        $result["data"] = $data;

        return response()->json($result);
    }

    public function store(Request $request) {
        //rules
        $rules=array(
            'name_id' => 'required',
            'name_en' => 'required',
            'type' => 'required',
            'order' => 'required',
            'icon' => $request->hasFile('icon') ? 'required|image|max:2000' : ''
        );
          
        //message error 
        $messages=array(
            // 'product.required' => 'Product required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
            $table = new Menu;

            $table->parent_id = $request->parent;
            $table->name_id = $request->name_id;
            $table->name_en = $request->name_en;
            $table->type = $request->type;
            $table->ref_id = $request->ref;
            $table->order = $request->order;
            
            $stringURL = str_replace('&', '', $request->name_en);
            $stringURL = str_replace(' ', '_', $stringURL);
            $stringURL = urlencode($stringURL);

            $table->link = strtolower($stringURL);

            //set image unique name
            if($request->hasFile('icon')) {
                $image = $request->icon;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/menu_icon', $filenameToStore);

                $table->icon = $filenameToStore;
            } else {
                $table->icon = "noicon.jpg";
            }
           
            if($table->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Data Submited.'
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Submit Failed.'
                ]);
            }  
        }
    }

    public function destroy(Request $request) {
        $deleted_child = Menu::where("parent_id", $request->id)->delete();

        $deleted = Menu::where("id", $request->id)->delete();

        if($deleted) {
            return response()->json([
                'status' => 'success',
                'message' => 'Record Deleted.'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
        $record = Menu::where('id', $request->id)->first();
        
        return response()->json($record);
    }

    public function update(Request $request) {
        $updated = 0;

        //rules
        $rules=array(
            'id' => 'required',
            'name_id' => 'required',
            'name_en' => 'required',
            'type' => 'required',
            'order' => 'required',
            'icon' => $request->hasFile('icon') ? 'required|image|max:2000' : ''
        );
          
        //message error 
        $messages=array(
            //'id.required' => 'ID required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
            $stringURL = str_replace(' ', '_', $request->name_en);
            $stringURL = urlencode($stringURL);

            //set image unique name
            if($request->hasFile('icon')) {
                // delete old file
                $image = Menu::select ('icon')->where('id', '=', $request->id)->first();
                $file_name = $image['icon'];

                if(Storage::exists('public/menu_icon/'.$file_name)) {
                    $delete_file = Storage::delete('public/menu_icon/'.$file_name);
                }

                //save new file
                $image = $request->icon;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/menu_icon', $filenameToStore);

                $updated = Menu::where('id', $request->id)
                              ->update([
                                    'parent_id' => $request->parent,
                                    'name_id' => $request->name_id,
                                    'name_en' => $request->name_en,
                                    'type' => $request->type,
                                    'ref_id' => $request->ref,
                                    'order' => $request->order,
                                    'icon' => $filenameToStore,
                                    'link' => strtolower($stringURL)
                                ]);
            } else {
                $updated = Menu::where('id', $request->id)
                              ->update([
                                    'parent_id' => $request->parent,
                                    'name_id' => $request->name_id,
                                    'name_en' => $request->name_en,
                                    'type' => $request->type,
                                    'ref_id' => $request->ref,
                                    'order' => $request->order,
                                    'link' => strtolower($stringURL)
                                ]);
            }            
        }

        if($updated) {
            return response()->json([
                'status' => 'success',
                'message' => 'Data Updated.'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
