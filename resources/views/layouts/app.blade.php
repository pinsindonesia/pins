<!DOCTYPE html>
    <html>
      <head>
          <meta charset="utf-8"/>
          <title>Admin PINS INDONESIA</title>
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
          <meta name="csrf-token" content="{{ csrf_token() }}">
          <link rel="stylesheet" href="{{asset('template/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
          <link rel="stylesheet" href="{{asset('template/bower_components/font-awesome/css/font-awesome.min.css')}}">
          <link rel="stylesheet" href="{{asset('template/bower_components/Ionicons/css/ionicons.min.css')}}">
          <link rel="stylesheet" href="{{asset('template/dist/css/AdminLTE.min.css')}}">
          <link rel="stylesheet" href="{{asset('template/dist/css/skins/_all-skins.min.css')}}">
          <link rel="stylesheet" href="{{asset('template/bower_components/jvectormap/jquery-jvectormap.css')}}">
          <link rel="stylesheet" href="{{asset('template/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
          <link rel="stylesheet" href="{{asset('template/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
          <link rel="stylesheet" href="{{asset('template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
          <link rel="stylesheet" type="text/css" href="{{asset('template/plugins/datatable/datatables.min.css')}}"/>
          <link rel="stylesheet" href="{{asset('template/plugins/validation/css/screen.css')}}">
          <link rel="stylesheet" href="{{asset('template/plugins/Rich-Text-Editor/src/richtext.min.css')}}">
          <link rel="stylesheet" href="{{asset('template/plugins/bootstrap-select/css/bootstrap-select.min.css')}}">
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
          <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>
          <link rel="shortcut icon" href="{{asset('img/icon.jpeg')}}"/>
      </head>
    <body class="hold-transition skin-red-light sidebar-mini">
        <div class="wrapper">
          @include('inc.header')
          @include('inc.sidebar')
          <div class="content-wrapper">
            @yield('content')
          </div>
          <footer class="main-footer">
            <div class="pull-right hidden-xs">
            </div>
            Copyright © All Rights Reserved 2018. <strong>PT PINS INDONESIA</strong>
          </footer>
        </div>

        <div class="loading" style="display: none;">Loading&#8230;</div>
        <script src="{{asset('template/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('template/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="{{asset('template/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('template/bower_components/raphael/raphael.min.js')}}"></script>
        <script src="{{asset('template/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{asset('template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
        <script src="{{asset('template/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
        <script src="{{asset('template/bower_components/moment/min/moment.min.js')}}"></script>
        <script src="{{asset('template/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        <script src="{{asset('template/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
        <script src="{{asset('template/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('template/bower_components/fastclick/lib/fastclick.js')}}"></script>
        <script src="{{asset('template/dist/js/adminlte.min.js')}}"></script>
        <script src="{{asset('template/dist/js/demo.js')}}"></script>
        <script type="text/javascript" src="{{asset('template/plugins/datatable/datatables.min.js')}}"></script>
        <script src="{{asset('template/plugins/validation/dist/jquery.validate.js')}}"></script>
        <script src="{{asset('template/plugins/treeview/bootstrap-treeview.js')}}"></script>
        <script src="{{asset('template/plugins/Rich-Text-Editor/src/jquery.richtext.min.js')}}"></script>
        <script src="{{asset('template/plugins/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('template/plugins/canvasjs/js/topup.js')}}"></script>
        <script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
        @section('assets')
        @show
        @section('assets_header')
        @show
    </body>
  </html>