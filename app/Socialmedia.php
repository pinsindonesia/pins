<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Socialmedia extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'link', 'photo', 'description'
    ];

    protected $dates = ['deleted_at'];
}
