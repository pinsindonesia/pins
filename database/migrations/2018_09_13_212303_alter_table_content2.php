<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableContent2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->dropColumn('text_1_id');
            $table->dropColumn('text_2_id');
            $table->dropColumn('text_3_id');
            $table->dropColumn('text_1_en');
            $table->dropColumn('text_2_en');
            $table->dropColumn('text_3_en');
            $table->dropColumn('image_1');
            $table->dropColumn('image_2');
            $table->dropColumn('image_3');
            $table->dropColumn('video_1');
        });

        Schema::table('contents', function (Blueprint $table) {
            $table->text('text_1_id')->nullable();
            $table->text('text_2_id')->nullable();
            $table->text('text_3_id')->nullable();
            $table->text('text_1_en')->nullable();
            $table->text('text_2_en')->nullable();
            $table->text('text_3_en')->nullable();
            $table->string('image_1')->nullable();
            $table->string('image_2')->nullable();
            $table->string('image_3')->nullable();
            $table->string('video_1')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
