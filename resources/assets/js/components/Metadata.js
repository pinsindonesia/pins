import React, { Component } from 'react'
import MetaTags from 'react-meta-tags';

export default class Metadata extends Component {
    render() {
        const { title, desc, img, url } = this.props

        return (
            <MetaTags>
                <title>{title} - PINS Indonesia</title>

                <meta name="description" content={desc} />
                <meta name="image" content={img} />

                <meta itemProp="name" content={title} />
                <meta itemProp="description" content={desc} />
                <meta itemProp="image" content={img} />

                <meta name="og:title" content={title}/>
                <meta name="og:description" content={desc} />
                <meta name="og:image" content={img} />
                <meta name="og:url" content={url} />
                <meta name="og:site_name" content="PINS Indonesia"/>
                <meta name="fb:admins" content="1429031130694826"/>
                <meta name="fb:app_id" content="239233773363360"/>
                <meta name="og:type" content="website"/>
            </MetaTags>
        );
    }
}
