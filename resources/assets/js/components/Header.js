import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { Helpers } from '../utils'

import { fetchMenu, setMenuStick } from '../redux/actions/menuActions'
import { selectLanguage } from '../redux/actions/languageActions'

@connect((store) => {
    return {
        menus: store.menu.menu,
        menuFetched: store.menu.fetched,
        active: store.menu.active,
        stick: store.menu.stick,
        lang: Helpers.isEmptyString(localStorage.getItem('language')) ? store.language.language : localStorage.getItem('language')
    }
})
class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            windowWidth: window.innerWidth
        };

        this.handleResize = this.handleResize.bind(this)
        this.handleScroll = this.handleScroll.bind(this)
    }

    isMenuActive(link) {
        return this.props.active && this.props.active.match(link) ? 'active' : ''
    }

    onMenuClick() {
        if ($(window).scrollTop() > 100)
            $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo')
    }

    handleResize() {
        this.setState({windowWidth: window.innerWidth});
    }

    handleScroll() {
        let s = $("#sticker")
        let pos = s.position()

        let windowpos = $(window).scrollTop() > 70
        if (windowpos > pos.top) this.props.dispatch(setMenuStick(true))
        else this.props.dispatch(setMenuStick(false))
    }

    handleNavBar() {
        let mainmenu = $(".main-menu ul.navbar-nav li ")

        mainmenu.on('click', () => {
            mainmenu.removeClass("active")
            $(this).addClass("active")
        })

        $(".navbar-collapse a").on('click', () => {
            $(".navbar-collapse.collapse").removeClass('in')
        })
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
        window.addEventListener('scroll', this.handleScroll)
        this.handleNavBar()
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
        window.removeEventListener('scroll', this.handleScroll)
    }

    componentWillMount() {
        this.props.dispatch(fetchMenu())
    }

    setLanguage(lang) {
        this.props.dispatch(selectLanguage(lang))
        window.location.reload()
    }

    isLanguageActive(lang) {
        return this.props.lang == lang ? 'active' : ''
    }

    render() {
        const { stick, menus, lang } = this.props

        let logo = Helpers.fHref('/img/logo' + (stick ? '' : '-white') + '.png')
        if (this.state.windowWidth < 768) logo = Helpers.fHref('/img/logo.png')

        return (
            <div>
                <div id='sticker' className={'header-area ' + (stick ? 'stick' : '')}>
                <div className='container'>
                    <div className='row'>
                    <div className='col-md-12 col-sm-12'>
                        <nav className='navbar navbar-default'>
                        <div className='navbar-header'>
                            <button type='button' className='navbar-toggle collapsed' data-toggle='collapse' data-target='.bs-example-navbar-collapse-1' aria-expanded='false'>
                                <span className='sr-only'>Toggle navigation</span>
                                <span className='icon-bar'></span>
                                <span className='icon-bar'></span>
                                <span className='icon-bar'></span>
                            </button>

                            <a className='navbar-brand page-scroll sticky-logo' href='/#/'>
                                <img className='logo' src={logo} alt='' title='' />
                            </a>
                        </div>

                        <div className='collapse navbar-collapse main-menu bs-example-navbar-collapse-1' id='navbar-example'>
                            <ul className='nav navbar-nav navbar-right'>
                                {menus.map((menu, index) => {
                                    if (menu.child.length == 0) {
                                        return (
                                            <li key={index} className={this.isMenuActive(menu.href)}>
                                                <a className='page-scroll' href={Helpers.fHref('/#/nav/' + menu.href)} onClick={() => this.onMenuClick()}>{menu.title}</a>
                                            </li>
                                        )
                                    } else {
                                        return (
                                            <li key={index} className={this.isMenuActive(menu.href)}>
                                                <a className='page-scroll' href='#' data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{menu.title}</a>

                                                <div className='full-dropdown-menu dropdown-menu row wow fadeIn'>
                                                    {menu.child.map((child, index) => {
                                                        let space = 12 % menu.child.length == 0 ? (12 / menu.child.length) : 4

                                                        const hasChild = child.child.length > 0
                                                        const href = '/#/nav/' + child.href

                                                        if (menu.type == 2 && menu.child.length > 4) {
                                                            space = 2
                                                            const diff = (menu.child.length / 6) >> 0
                                                            const sId = 6 * diff

                                                            let offset = (((menu.child.length - 6) < 0) && index == 0) ? 1 : (index == sId) ? (6 * (diff + 1)) - menu.child.length : 0

                                                            return (
                                                                <div key={index} className={'col-md-offset-' + offset + ' col-md-' + space +' col-xs-12'}>
                                                                    <a className={'dropdown-item item-parent ' + (child.title.length > 16 ? 'save-length' : '')} href={Helpers.fHref(href)}>
                                                                    <img className='item-icon' src={Helpers.imgUrlMenu(child.icon)} />
                                                                    {child.title}</a>
                                                                </div>
                                                            )
                                                        } else {
                                                            return (
                                                                <div key={index} className={'col-md-' + space +' col-xs-12'}>
                                                                    <a className={'dropdown-item ' + (!hasChild ? 'item-parent' : '')} href={Helpers.fHref(href)}>
                                                                    <img className='item-icon' src={Helpers.imgUrlMenu(child.icon)} />
                                                                    {child.title}</a>

                                                                    {hasChild && <ul className='dropdown-submenu'>
                                                                        {child.child.map((gchild, index) =>
                                                                            <li key={index}><a className='dropdown-item dropdown-subitem' href={Helpers.fHref('/#/nav/' + gchild.href)}>{gchild.title}</a></li>
                                                                        )}
                                                                    </ul>}

                                                                </div>
                                                            )
                                                        }
                                                    })}
                                                </div>
                                            </li>
                                        )
                                    }
                                })}

                                <li>
                                    <a className='page-scroll dropdown-toggle' data-toggle='dropdown' href='#'role='button' aria-haspopup='true' aria-expanded='false'>
                                        <img className='menu-flag' src={Helpers.fHref('/img/frontend/flags/flag-'+lang+'.svg')} alt='id' title='Indonesia'/>
                                    </a>
                                    <ul className='dropdown-menu menu-lang'>
                                        <li><a className={'dropdown-item ' + this.isLanguageActive('id')} href='#' onClick={e=>{
                                        e.preventDefault()
                                        e.stopPropagation()
                                        this.setLanguage('id')
                                    }} ><img className='item-flag' src={Helpers.fHref('/img/frontend/flags/flag-id.svg')} alt='id' title='Indonesia'/>Indonesia</a></li>
                                        <li><a className={'dropdown-item ' + this.isLanguageActive('en')} href='#' onClick={e=>{
                                        e.preventDefault()
                                        e.stopPropagation()
                                        this.setLanguage('en')
                                    }} ><img className='item-flag' src={Helpers.fHref('/img/frontend/flags/flag-en.svg')} alt='id' title='English'/>English</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        </nav>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Header)
