<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Area;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	}
	
    public function index()
    {
        return view('pages/area');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', '');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = Area::where('name', 'LIKE', '%'.$filter.'%')->get();
    	$list_filtered = Area::where('name', 'LIKE', '%'.$filter.'%')
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
			$action = '
						<center>
							<button class="btn btn-sm btn-warning" onclick="_edit(\''.$value['id'].'\');" title="Edit"><i class="fa fa-edit"></i></button>
							<button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete"><i class="fa fa-trash"></i></button>
						</center>
						';

            $featured = $value['featured'] == 'true' ? '<br><span class="label label-primary">Featured</span>' : '';

		    array_push($data, 
				array(
					'<b>Name</b> ' . $value['name'] . '<br/>' . '<b>Location</b> '. $value['location'] . '<br/>' . '<b>Email</b> '. $value['email'] . '<br/>' . '<b>Telp</b> '. $value['telp'] . '<br/>' . '<b>Fax</b> '. $value['fax'] . $featured,
					$action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
	}
	
	public function store(Request $request) {
    	//rules
        $rules=array(
        	'name' => 'required',
            'location' => 'required'
        );
          
        //title error 
        $titles=array(
        	'name.required' => 'Name required.',
            'location.required' => 'location required.'
        );

		$validator = Validator::make($request->all(),$rules,$titles);
		
        if($validator->fails()) {
            $titles=$validator->messages();
            $errors=$titles->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			$table = new Area;
            $table->name = $request->name;
            $table->location = $request->location;
            $table->email = $request->email;
            $table->telp = $request->telp;
            $table->fax = $request->fax;
            $table->featured = $request->featured == 'true' ? 'true' : 'false';
			
            if($table->save()) {
	        	return response()->json([
		            'status' => 'success',
		            'message' => 'Form Submited.'
		        ]);
	        } else {
	        	return response()->json([
                    'status' => 'error',
                    'message' => 'Submit Failed.'
                ]);
	        }
        }
	}

	public function destroy(Request $request) {
    	$deleted = Area::where('id', $request->id)->delete();

        if($deleted) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Record Deleted.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
		$record = Area::where('id', $request->id)->first();
    	return response()->json($record);
    }
	
	public function update(Request $request) {
    	$updated = 0;

    	//rules
        $rules=array(
        	'id' => 'required',
        	'name' => 'required',
            'location' => 'required'
        );
          
        //title error 
        $titles=array(
        	'id.required' => 'ID required.',
        	'name.required' => 'Name required.',
            'location.required' => 'Location required.'
        );

        $validator = Validator::make($request->all(),$rules,$titles);
        if($validator->fails()) {
            $titles=$validator->messages();
            $errors=$titles->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			//check location existing
	    	$exist = Area::where('name', $request->name)->first();

            $featured = $request->featured == "true" ? "true" : "false";

	    	if($exist) {
                if($exist['name'] == $request->name_old) {
                    $updated = Area::where('id', $request->id)
                                    ->update([
                                            'name' => $request->name,
                                            'location' => $request->location,
                                            'email' => $request->email,
                                            'telp' => $request->telp,
                                            'fax' => $request->fax,
                                            'featured' => $featured
                                        ]);
                } else {     
        			return response()->json([
    	                'status' => 'error',
    	                'message' => 'The location has already been taken.'
    	            ]);
                }
	    	} else {
	    		$updated = Area::where('id', $request->id)
		                            ->update([
		                                    'name' => $request->name,
		                                    'location' => $request->location,
                                            'email' => $request->email,
                                            'telp' => $request->telp,
                                            'fax' => $request->fax,
                                            'featured' => $featured
		                                ]);
	    	}
        }

        if($updated) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Form Updated.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
