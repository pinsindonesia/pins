<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Admin PINS INDONESIA</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{asset('template/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('template/bower_components/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('template/bower_components/Ionicons/css/ionicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('template/dist/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{asset('template/plugins/iCheck/square/blue.css')}}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="{{asset('img/logo.png')}}"/>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
          <div class="login-logo">
            <a href="/admin">
                <img src="{{asset('img/logo.png')}}" alt="Pin's"/>
            </a>
          </div>
            @yield('auth')
        </div>
        <script src="{{asset('template/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('template/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('template/plugins/iCheck/icheck.min.js')}}"></script>
        <script>
          $(function () {
            $('input').iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%'
            });
          });
        </script>
    </body>
</html>
