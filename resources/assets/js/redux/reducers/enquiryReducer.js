export default function reducer(state={
    enquiry: {},
    adding: false,
    added: false,
    error: null,
}, action) {
    switch (action.type) {
        case 'ADD_ENQUIRY_PROCESSING': {
            return {...state, adding: true }
        }
        case 'ADD_ENQUIRY_REJECTED': {
            return {...state, adding: false, error: action.payload }
        }
        case 'ADD_ENQUIRY_FULFILLED': {
            return {...state, adding: false, added: true, enquiry: action.payload}
        }
        case 'ADD_ENQUIRY_RESET': {
            return state
        }
    }

    return state
}
