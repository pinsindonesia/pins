import React, { Component } from 'react'
import { connect } from 'react-redux'

import About from '../components/About'
import LatestNews from '../components/LatestNews'
import Portfolio from '../components/Portfolio'
import Slider from '../components/Slider'

import { setActiveMenu } from '../redux/actions/menuActions'
import { fetchHomeSlider } from '../redux/actions/sliderActions'
import Analytic from '../components/Analytic';

@connect((store) => {
    return {
        sliders: store.slider.sliders
    }
})
export default class Home extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.dispatch(fetchHomeSlider())
    }

    componentDidMount() {
        this.props.dispatch(setActiveMenu(this.props.href))
    }

    render() {
        return (
            <div>
                <Analytic location={this.props.location} />
                <Slider home sliders={this.props.sliders}/>
                <About />
                <Portfolio />
                <LatestNews />
            </div>
        )
    }
}
