<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Team;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	}
	
    public function index()
    {
        return view('pages/team');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', 'photo', '');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = Team::where('name', 'LIKE', '%'.$filter.'%')->get();
    	$list_filtered = Team::where('name', 'LIKE', '%'.$filter.'%')
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
			$action = '
						<center>
							<button class="btn btn-sm btn-warning" onclick="_edit(\''.$value['id'].'\');" title="Edit"><i class="fa fa-edit"></i></button>
							<button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete"><i class="fa fa-trash"></i></button>
						</center>
					';

            $group_name = $value['group_name'] == 1 ? 'Commissioner' : 'Director' ;

		    array_push($data, 
				array(
					'<b>Name</b> ' . $value['name'] . '<br/>' . '<b>Group</b> '. $group_name . '<br/>' . '<b>Title ID</b> '. $value['title_id'] . '<br/>' . '<b>Title EN</b> '. $value['title_en'] . '<br/>' . '<b>Detail ID</b> '. $value['details_id'] . '<br/>' . '<b>Detail EN</b> '. $value['details_en'],
                    '<img id="image_detail" alt="Image" style="width: 120px; height: 150px;" src="'.asset('storage/team/'.$value['photo']).'"/>',
					$action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
	}
	
	public function store(Request $request) {
    	//rules
        $rules=array(
            'name' => 'required',
            'group_name' => 'required',
            'title_id' => 'required',
            'title_en' => 'required',
            'photo' => 'required|image|max:2000',
            'details_id' => 'required',
            'details_en' => 'required'
        );
          
        //title error 
        $titles=array(
            //'name.required' => 'Name required.'
        );

		$validator = Validator::make($request->all(),$rules,$titles);
		
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			$table = new Team;

            $table->name = $request->name;
            $table->group_name = $request->group_name;
            $table->title_id = $request->title_id;
            $table->title_en = $request->title_en;
            $table->details_id = $request->details_id;
            $table->details_en = $request->details_en;

            //set image unique name
            if($request->hasFile('photo')) {
                $image = $request->photo;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/team', $filenameToStore);

                $table->photo = $filenameToStore;
            } else {
                $table->photo = "";
            }
			
            if($table->save()) {
	        	return response()->json([
		            'status' => 'success',
		            'message' => 'Form Submited.'
		        ]);
	        } else {
	        	return response()->json([
                    'status' => 'error',
                    'message' => 'Submit Failed.'
                ]);
	        }
        }
	}

	public function destroy(Request $request) {
    	$deleted = Team::where('id', $request->id)->delete();

        if($deleted) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Record Deleted.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
		$record = Team::where('id', $request->id)->first();
    	return response()->json($record);
    }
	
	public function update(Request $request) {
    	$updated = 0;

    	//rules
        $rules=array(
        	'id' => 'required',
            'name' => 'required',
            'group_name' => 'required',
            'title_id' => 'required',
            'title_en' => 'required',
            'photo' => $request->hasFile('photo') ? 'required|image|max:2000' : '',
            'details_id' => 'required',
            'details_en' => 'required'
        );
          
        //title error 
        $titles=array(
        	//'id.required' => 'ID required.',
        );

        $validator = Validator::make($request->all(),$rules,$titles);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
			//set image unique name
            if($request->hasFile('photo')) {
                // delete old file
                $image = Team::select ('photo')->where('id', '=', $request->id)->first();
                $file_name = $image['photo'];

                if(Storage::exists('public/team/'.$file_name)) {
                    $delete_file = Storage::delete('public/team/'.$file_name);
                }

                //save new file
                $image = $request->photo;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/team', $filenameToStore);

                $updated = Team::where('id', $request->id)
                              ->update([
                                    'name' => $request->name,
                                    'title_id' => $request->title_id,
                                    'title_en' => $request->title_en,
                                    'details_id' => $request->details_id,
                                    'details_en' => $request->details_en,
                                    'photo' => $filenameToStore
                                ]);
            } else {
                $updated = Team::where('id', $request->id)
                              ->update([
                                    'name' => $request->name,
                                    'title_id' => $request->title_id,
                                    'title_en' => $request->title_en,
                                    'details_id' => $request->details_id,
                                    'details_en' => $request->details_en,
                                ]);
            }  
        }

        if($updated) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Form Updated.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
