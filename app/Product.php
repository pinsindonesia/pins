<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'content'
    ];

    protected $dates = ['deleted_at'];

    public function menu() {
        return $this->hasMany('App\Menu', 'ref_id');
    }

    public function overview() {
        return $this->belongsTo('App\Content', 'overview_id');
    }

    public function whyUs() {
        return $this->belongsTo('App\Content', 'why_us_id');
    }

    public function product() {
        return $this->belongsTo('App\Content', 'product_id');
    }

    public function cased() {
        return $this->belongsTo('App\Content', 'cased_id');
    }

    public function getIsFeaturedSavedAttribute() {
        return $this->menu->first()->parent_id == 2;
    }

    public function scopeFeatured($query) {
        return $query->where('featured', 'true');
    }

    public function scopeFeaturedSaved($query) {
        return $query
            ->where('featured', 'true')
            ->join('menus', function($join) {
                $join->on('products.id', '=', 'menus.ref_id')
                    ->where('parent_id', 2);
            })->orderBy('order', 'asc')
            ->select('products.*');
    }

    public function partner() {
        return Partner::where('product_id', 'LIKE', '%'. $this->id .'%');
    }
}
