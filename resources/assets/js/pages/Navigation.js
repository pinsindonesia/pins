import React, { Component } from 'react'
import { connect } from 'react-redux'

import Home from './Home'
import NewsFull from './NewsFull'
import ContentContact from './ContentContact';
import ContentOther from './ContentOther';
import ContentProduct from './ContentProduct'
import ContentTeam from './ContentTeam';

import { fetchMenuDetails } from '../redux/actions/menuActions';
import ContentPartner from './ContentPartner';

@connect((store) => {
    return {
        menu: store.menu.menuDetail,
        fetched: store.menu.mdFetched
    }
})export default class Navigation extends Component {
    constructor(props) {
        super(props)
    }

    getMenuDetails(link) {
        this.props.dispatch(fetchMenuDetails(link || this.props.match.params.link))
        $('#navbar-example').removeClass("show")
    }

    componentWillMount() {
        this.getMenuDetails()
    }

    componentWillReceiveProps(nextProps) {
        const then = this.props.match.params.link
        const now = nextProps.match.params.link

        if (then != now) this.getMenuDetails(nextProps.match.params.link)
    }

    render() {
        const { menu, fetched } = this.props

        if (fetched) {
            switch (menu.type) {
                case 0: return <ContentOther href={menu.href} location={this.props.location} />
                case 1: return <Home href={menu.href} location={this.props.location} />
                case 2: return <ContentProduct href={menu.href} location={this.props.location} />
                case 3: return <NewsFull href={menu.href} location={this.props.location} />
                case 4: return <ContentTeam href={menu.href} location={this.props.location} />
                case 5: return <ContentPartner href={menu.href} location={this.props.location} />
                case 6: return <ContentContact href={menu.href} location={this.props.location} />
            }
        }

        return <div id='preloader' />
    }
}
