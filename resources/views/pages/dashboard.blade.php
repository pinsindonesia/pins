@extends('layouts.app')
@section('content')

<section class="content">
  <div class="row">
      <div class="col-lg-2 col-xs-6">
        <a href="/admin/user" class="small-box bg-teal" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>USER</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-user" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
    </div>

    <div class="col-lg-2 col-xs-6">
        <a href="/admin/company" class="small-box bg-lime" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>COMPANY</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-institution" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
      </div>

      <div class="col-lg-2 col-xs-6">
        <a href="/admin/menu" class="small-box bg-orange" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>MENU</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-bars" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
      </div>

      <div class="col-lg-2 col-xs-6">
          <a href="/admin/content" class="small-box bg-olive" style="height: 150px;">
            <div class="inner" style="position: absolute;bottom: -12px;">
              <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>CONTENT</strong></p>
            </div>
            <div class="icon">
              <i class="fa fa-file-image-o" style="color: rgba(255,255,255,0.9);"></i>
            </div>
          </a>
      </div>

    <div class="col-lg-2 col-xs-6">
      <a href="/admin/product" class="small-box bg-aqua" style="height: 150px;">
        <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>PRODUCT</strong></p>
        </div>
        <div class="icon">
          <i class="fa fa-cube" style="color: rgba(255,255,255,0.9);"></i>
        </div>
      </a>
    </div>

    <div class="col-lg-2 col-xs-6">
        <a href="/admin/article" class="small-box bg-blue" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>ARTICLE</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-newspaper-o" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
    </div>

  </div>
  <!-- /.row -->
  <div class="row">

    <div class="col-lg-2 col-xs-6">
        <a href="/admin/partner" class="small-box bg-yellow" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>PARTNER</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-handshake-o" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
    </div>

    <div class="col-lg-2 col-xs-6">
        <a href="/admin/team" class="small-box bg-red" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>SOCIAL MEDIA</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-wechat" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
    </div>

    <div class="col-lg-2 col-xs-6">
        <a href="/admin/area" class="small-box bg-blue" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>AREA</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-globe" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
    </div>
    
    <div class="col-lg-2 col-xs-6">
        <a href="/admin/socialmedia" class="small-box bg-maroon" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>TEAM</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-group" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
    </div>    

    <div class="col-lg-2 col-xs-6">
        <a href="/admin/enquiry" class="small-box bg-green" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>ENQUIRY</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-send-o" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
      </div>

    <div class="col-lg-2 col-xs-6">
        <a href="/admin/comment" class="small-box bg-navy" style="height: 150px;">
          <div class="inner" style="position: absolute;bottom: -12px;">
            <p class="pull-left" style="color:rgba(255,255,255,1);"><strong>COMMENT</strong></p>
          </div>
          <div class="icon">
            <i class="fa fa-comments-o" style="color: rgba(255,255,255,0.9);"></i>
          </div>
        </a>
      </div>
  </div>

  <br><br>
  <div class="row hidden-sm hidden-xs" style="padding-left: 40px;">
      <div class="col-md-5">
        <span class="col-md-12" style="text-align: center;"><strong>Website Visitor</strong></span><br><br>
        <canvas id="visitor_graph" width="100%" height="250" style="background: white; padding: 40px 15px 0px 0px;"></canvas>
      </div>
      <div class="col-md-offset-1 col-md-5">
        <span class="col-md-6" style="text-align: center;"><strong>Article Visitor</strong></span>
        <span class="col-md-6" style="text-align: center;">
            <select class="form-control" id="article_list" name="article_list" onchange="getArticle(this.value);">
                
            </select>
        </span><br><br>
        <canvas id="article_graph" width="100%" height="250" style="background: white; padding: 40px 15px 0px 0px;"></canvas>
      </div>
      <br><br>
  </div>
  <br><br>

</section>
@endsection

@section('assets')
<script>
    jQuery(document).ready(function () {
        //get article list
        $.ajax({
            kind: 'GET',
            url: "admin/dashboard/article_list", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {}, 
            success: function(response) {
                $("#article_list").html("");
                $("#article_list").append("<option value=\"\">-- choose article --</option>");
                for(var item of response.data) {
                    $("#article_list").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                }
            }
        });

        //set canvas size
        var vctrl = document.getElementById("visitor_graph"),
            vparentSize = vctrl.parentNode.getBoundingClientRect();

        vctrl.width  = vparentSize.width;
        vctrl.height = vparentSize.height;

        var actrl = document.getElementById("article_graph"),
            aparentSize = actrl.parentNode.getBoundingClientRect();

        actrl.width  = aparentSize.width;
        actrl.height = aparentSize.height;

        //set visitor graph
        $.ajax({
            type: 'GET',
            url: "admin/dashboard/visitor",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {},
            success: function (response) {
              console.log(response);
                var chartData = {
                    node: "visitor_graph",
                    dataset: response.valuey,
                    labels: response.valuex,
                    pathcolor: "#288ed4",
                    fillcolor: "red",
                    xPadding: 0,
                    yPadding: 0,
                    ybreakperiod: 5
                };

                drawlineChart(chartData);
            }
        });
    });  

    //set article visitor graph
    function getArticle(id) {
        var canvas = document.getElementById("article_graph");
        var context = canvas.getContext("2d");

        context.clearRect(0, 0, canvas.width, canvas.height);

        $.ajax({
            type: 'GET',
            url: "admin/dashboard/article",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                var chartData = {
                    node: "article_graph",
                    dataset: response.valuey,
                    labels: response.valuex,
                    pathcolor: "#288ed4",
                    fillcolor: "red",
                    xPadding: 0,
                    yPadding: 0,
                    ybreakperiod: 5
                };

                drawlineChart(chartData);
            }
        });
    }
</script>
@endsection