export default function reducer(state={
    selectedTeam: {},
    selected: false,
}, action) {
    switch (action.type) {
        case 'SELECT_TEAM': {
            return {...state, selected: true, selectedTeam: action.payload}
        }
    }

    return state
}
