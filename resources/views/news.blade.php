<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="/frontend/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/frontend/lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
        <link href="/frontend/lib/owlcarousel/owl.carousel.min.css" rel="stylesheet">
        <link href="/frontend/lib/owlcarousel/owl.theme.default.min.css" rel="stylesheet">
        <link href="/frontend/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="/frontend/lib/animate/animate.min.css" rel="stylesheet">
        <link href="/frontend/lib/venobox/venobox.css" rel="stylesheet">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <title>{{ $title }} - PINS Indonesia</title>
        <meta name="description" content="{{$desc}}" />
        <meta name="image" content="{{$img}}" />
        <meta itemProp="name" content="{{$title}}" />
        <meta itemProp="description" content="{{$desc}}" />
        <meta itemProp="image" content="{{$img}}" />
        <meta name="og:title" content="{{$title}}"/>
        <meta name="og:description" content="{{$desc}}" />
        <meta name="og:image" content="{{$img}}" />
        <meta name="og:url" content="{{$url}}" />
        <meta name="og:site_name" content="PINS Indonesia"/>
        <meta name="fb:admins" content="1429031130694826"/>
        <meta name="fb:app_id" content="239233773363360"/>
        <meta name="og:type" content="website"/>
    </head>
    <body>
        <div id="app"></div>
        <script src="/frontend/lib/wow/wow.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1hmrtC0PDyM5NtWKneEXV4TfLKK6GRR8"></script>
        <script crossorigin src="https://unpkg.com/universal-cookie@3/umd/universalCookie.min.js"></script>
        <script src="/js/app.js"></script>
    </body>
</html>
