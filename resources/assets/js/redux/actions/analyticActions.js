import Axios from 'axios'
import { Constants } from '../../utils/index';

export function addAnalytic(url, useragent) {
    return (dispatch) => {
        Axios.post(Constants.API_ANALYTIC_URL, {
            url: url,
            useragent: useragent
        })
        .then(res => {
            dispatch({ type: 'ADD_ANALYTIC_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'ADD_ANALYTIC_REJECTED', payload: err })
        })
    }
}
