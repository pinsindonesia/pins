import React, { Component } from 'react'

import SectionHeadline from './SectionHeadline'

export default class Pricing extends Component {
    render() {
        const basicFeatures = [
            {title: 'Online system', active: true},
            {title: 'Full Access', active: true},
            {title: 'Free Apps', active: true},
            {title: 'Multiple Slider', active: true},
            {title: 'Free Domain', active: false},
            {title: 'Support Unlimited', active: false},
            {title: 'Payment Online', active: false},
            {title: 'Cash back', active: false}
        ]

        const standardFeatures = [
            {title: 'Online system', active: true},
            {title: 'Full Access', active: true},
            {title: 'Free Apps', active: true},
            {title: 'Multiple Slider', active: true},
            {title: 'Free Domain', active: false},
            {title: 'Support Unlimited', active: true},
            {title: 'Payment Online', active: true},
            {title: 'Cash back', active: false}
        ]

        const premiumFeatures = [
            {title: 'Online system', active: true},
            {title: 'Full Access', active: true},
            {title: 'Free Apps', active: true},
            {title: 'Multiple Slider', active: true},
            {title: 'Free Domain', active: true},
            {title: 'Support Unlimited', active: true},
            {title: 'Payment Online', active: true},
            {title: 'Cash back', active: true}
        ]

        return (
            <div id='pricing' className='pricing-area area-padding'>
                <div className='container'>
                <SectionHeadline title='Pricing Table' />
                <div className='row'>
                    <PricingItem
                        type='Basic'
                        price='$56'
                        units='Hari'
                        features={basicFeatures}
                        submitText='Sign Up Now' />

                    <PricingItem
                        active
                        ribbonText='Best Sale'
                        type='Standard'
                        price='$78'
                        units='Hari'
                        features={standardFeatures}
                        submitText='Sign Up Now' />

                    <PricingItem
                        type='Premium'
                        price='$150'
                        units='Hari'
                        features={premiumFeatures}
                        submitText='Sign Up Now' />
                </div>
                </div>
            </div>
        );
    }
}

class PricingItem extends Component {
    render() {
        const {
            active, ribbonText,
            type, price, units,
            features, submitText
        } = this.props

        return (
            <div className='col-md-4 col-sm-4 col-xs-12'>
            <div className={'pri_table_list ' + (active ? 'active' : '')}>
                {ribbonText &&
                    <span className='saleon'>{ribbonText}</span>
                }
                <h3>{type} <br/> <span>{price} / {units}</span></h3>
                <ol>
                {features.map((feature, index) =>
                    <li key={index} className={feature.active ? 'check' : 'check cross'}>{feature.title}</li>
                )}
                </ol>
                <button>{submitText}</button>
            </div>
            </div>
        );
    }
}
