import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { HashRouter as Router, Switch, Route, Redirect} from 'react-router-dom'

import ButtonScrollToTop from './components/ButtonScrollToTop'
import Footer from './components/Footer'
import Header from './components/Header'

import Navigation from './pages/Navigation'
import NewsShow from './pages/NewsShow'

import store from './redux/store'

export default class Index extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Header />
                    <Switch>
                        <Route path="/nav/:link" component={Navigation} />
                        <Route path='/news/:id' component={NewsShow}/>
                        <Redirect from='*' to='/nav/home' />
                    </Switch>
                    <Footer />
                    <ButtonScrollToTop />
                </div>
            </Router>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Provider store={store}>
            <Index />
        </Provider>, document.getElementById('app'));
}
