<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTeamsTable extends Migration
{
    public function up()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->renameColumn('details', 'details_en');
            $table->text('details_id');
            $table->renameColumn('title', 'title_en');
            $table->string('title_id');
        });

    }

    public function down()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->renameColumn('details_en', 'details');
            $table->dropColumn('details_id');
            $table->renameColumn('title_en', 'title');
            $table->dropColumn('title_id');
        });
    }
}
