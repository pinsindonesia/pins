import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Helpers, Constants } from '../utils/index'

import Cookies from 'universal-cookie'
import { addAnalytic } from '../redux/actions/analyticActions'

@connect((store) => {
    return {
        analytic: store.analytic.analytic
    }
})
export default class Analytic extends Component {
    componentDidMount() {
        const cookies = new Cookies()
        let guid = cookies.get('guid')
        if (!guid) {
            guid = Helpers.guidGenerator();
            cookies.set('guid', guid, {path: '/'})
        }

        const url = Constants.BASE_URL + '/#' + this.props.location.pathname
        this.props.dispatch(addAnalytic(url, guid))
    }

    render() {
        return <div></div>;
    }
}
