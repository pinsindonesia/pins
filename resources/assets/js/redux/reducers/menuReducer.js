export default function reducer(state={
    menu: [],
    fetching: false,
    fetched: false,
    error: null,
    active: 'home',
    stick: false,
    menuDetail: {},
    mdFetching: false,
    mdFetched: false,
    mdError: null,
}, action) {
    switch (action.type) {
        case 'FETCH_MENU': {
            return {...state, fetching: true, fetched: false}
        }
        case 'FETCH_MENU_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }
        case 'FETCH_MENU_FULFILLED': {
            return {...state, fetching: false, fetched: true, menu: action.payload}
        }
        case 'SET_MENU_ACTIVE' : {
            return {...state, active: action.payload}
        }
        case 'SET_MENU_STICKED' : {
            return {...state, stick: action.payload}
        }
        case 'FETCH_MENU_DETAIL': {
            return {...state, mdFetching: true, mdFetched: false}
        }
        case 'FETCH_MENU_DETAIL_REJECTED': {
            return {...state, mdFetching: false, mdError: action.payload}
        }
        case 'FETCH_MENU_DETAIL_FULFILLED': {
            return {...state, mdFetching: false, mdFetched: true, menuDetail: action.payload}
        }
    }

    return state
}
