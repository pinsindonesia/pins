<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Partner;
use App\Product;

class PartnerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages/partner');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', '', '');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = Partner::where('name', 'LIKE', '%'.$filter.'%')->get();
    	$list_filtered = Partner::where('name', 'LIKE', '%'.$filter.'%')
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
            $products_ = $value['product_id'];
            $product_array = explode(",", $products_);

            $product_name = Product::select('title')->whereIn('id', $product_array)->get();

            $implode = array();
            $multiple = json_decode($product_name, true);
            foreach($multiple as $single)
                $implode[] = implode(', ', $single);

			$action = '
					  <center>'.
						'<button class="btn btn-sm btn-warning" onclick="_edit(\''.$value['id'].'\');" title="Edit"><i class="fa fa-edit"></i></button>
	                    <button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete"><i class="fa fa-trash"></i></button>
					  </center>
					';

		    array_push($data, 
				array(
                    '<b>Name</b> ' . $value['name'] . '<br/>' . '<b>Product</b> '. implode(', ', $implode) . '<br/>' . '<b>Description</b> '. $value['description'],
                    '<img id="image_detail" alt="Image" style="width: 75px; height: 75px;" src="'.asset('storage/partner/'.$value['image']).'"/>',
					$action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
    }

    public function product_list(Request $request) {
		$list = Product::all();
    	
    	$data = array();			
		foreach ( $list as $value ) {
			array_push($data, 
				array(
					$value['id'],
					$value['title']
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
    }

    public function store(Request $request) {
    	//rules
        $rules=array(
        	'product' => 'required',
            'name' => 'required',
            'image' => 'required|image|max:2000',
            'description' => 'required',
        );
          
        //message error 
        $messages=array(
        	// 'product.required' => 'Product required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
        	$table = new Partner;

            $table->product_id = $request->product;
            $table->name = $request->name;
            $table->description = $request->description;

            //set image unique name
            if($request->hasFile('image')) {
                $image = $request->image;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/partner', $filenameToStore);

                $table->image = $filenameToStore;
            } else {
            	$table->image = "";
            }
           
        	if($table->save()) {
	        	return response()->json([
		            'status' => 'success',
		            'message' => 'Form Submited.'
		        ]);
	        } else {
	        	return response()->json([
                    'status' => 'error',
                    'message' => 'Submit Failed.'
                ]);
	        }  
        }
    }

    public function destroy(Request $request) {
        $deleted = Partner::where("id", $request->id)->delete();

        if($deleted) {
            return response()->json([
                'status' => 'success',
                'message' => 'Record Deleted.'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function detail(Request $request) {
        $record = Partner::where('id', $request->id)->with("product2")->first();
        
        return response()->json($record);
    }

    public function update(Request $request) {
        $updated = 0;

        //rules
        $rules=array(
            'id' => 'required',
            'product' => 'required',
            'name' => 'required',
            'image' => $request->hasFile('image') ? 'required|image|max:2000' : '',
            'description' => 'required',
        );
          
        //message error 
        $messages=array(
            //'id.required' => 'ID required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
            //set image unique name
            if($request->hasFile('image')) {
                // delete old file
                $image = Partner::select ('image')->where('id', '=', $request->id)->first();
                $file_name = $image['image'];

                if(Storage::exists('public/partner/'.$file_name)) {
                    $delete_file = Storage::delete('public/partner/'.$file_name);
                }

                //save new file
                $image = $request->image;
                
                $fileNameWithExt = $image->getClientOriginalName();

                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

                $extension = $image->getClientOriginalExtension();

                $filenameToStore = $filename.'_'.time().'.'.$extension;
                
                $path = $image->storeAs('public/partner', $filenameToStore);

                $updated = Partner::where('id', $request->id)
                              ->update([
                                    'product_id' => $request->product,
                                    'name' => $request->name,
                                    'image' => $filenameToStore,
                                    'description' => $request->description
                                ]);
            } else {
                $updated = Partner::where('id', $request->id)
                              ->update([
                                    'product_id' => $request->product,
                                    'name' => $request->name,
                                    'description' => $request->description
                                ]);
            }            
        }

        if($updated) {
            return response()->json([
                'status' => 'success',
                'message' => 'Form Updated.'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
