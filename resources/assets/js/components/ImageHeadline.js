import React, { Component } from 'react'

export default class ImageHeadline extends Component {
    render() {
        const { img, title } = this.props

        return (
            <div id='home' className='slider-area'>
                <div className='bend niceties preview-2'>
                    <div ref='slider' id='ensign-nivoslider' className='slides nivoSlider singleSlider'>
                        <img className='nivo-main-image' src={img} alt='' />
                        <div className='nivo-caption' style={{display: 'block'}}>
                            <div className='container fit-center'>
                                <div className='row'>
                                    <div className='col-md-12 col-sm-12 col-xs-12'>
                                        <div className='slider-content'>
                                            <div className='layer-1-1 wow fadeInDown' data-wow-duration='2s' data-wow-delay='.2s'>
                                                <h1 className='title1'>{title}</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
