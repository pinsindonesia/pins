import Axios from 'axios'
import { Constants, Helpers } from '../../utils';

export function fetchHomeSlider() {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_SLIDERS_URL))
        .then(res => {
            let sliders = [];
            res.data.map(item => {
                item.img = Helpers.imgUrlProduct(item.img)
                // item.buttonText = 'Read More'
                sliders.push(item)
            })
            dispatch({ type: 'FETCH_SLIDER_FULFILLED', payload: sliders })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_SLIDER_REJECTED', payload: err })
        })
    }
}

export function fetchNewsSlider() {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_NEWS_FEATURED_URL))
        .then(res => {
            let sliders = [];
            res.data.map(nws => {
                nws.key = 'news/' + nws.id
                nws.buttonText = 'Read More'
                nws.img = Helpers.imgUrlNews(nws.img)
                sliders.push(nws)
            })

            dispatch({ type: 'FETCH_SLIDER_FULFILLED', payload: sliders })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_SLIDER_REJECTED', payload: err })
        })
    }
}
