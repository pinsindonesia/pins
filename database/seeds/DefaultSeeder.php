<?php

use Illuminate\Database\Seeder;

class DefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notExist = 0;

        $about;

        //default menu
        $notExist = DB::table('menus')->where('link', 'home')->doesntExist();
        if($notExist) {
            (new App\Menu([
                'name_id' => 'Beranda',
                'name_en' => 'Home',
                'type' => 1,
                'link' => 'home',
                'order' => 1,
            ]))->save();
        }

        $notExist = DB::table('menus')->where('link', 'product-services')->doesntExist();
        if($notExist) {
            (new App\Menu([
                'name_id' => 'Produk & Layanan',
                'name_en' => 'Product & Services',
                'type' => 2,
                'link' => 'product-services',
                'order' => 20,
            ]))->save();
        }

        $notExist = DB::table('menus')->where('link', 'industries')->doesntExist();
        if($notExist) {
            (new App\Menu([
                'name_id' => 'Industri',
                'name_en' => 'Industries',
                'type' => 2,
                'link' => 'industries',
                'order' => 21,
            ]))->save();
        }

        $notExist = DB::table('menus')->where('link', 'news')->doesntExist();
        if($notExist) {
            (new App\Menu([
                'name_id' => 'Berita',
                'name_en' => 'News',
                'type' => 3,
                'link' => 'news',
                'order' => 30,
            ]))->save();
        }

        $notExist = DB::table('menus')->where('link', 'about')->doesntExist();
        if($notExist) {
            $about = new App\Menu([
                'name_id' => 'Tentang Kami',
                'name_en' => 'About Us',
                'type' => 7,
                'link' => 'about',
                'order' => 40,
            ]);
            $about->save();
        }

        $notExist = DB::table('menus')->where('link', 'team')->doesntExist();
        if($notExist) {
            (new App\Menu([
                'name_id' => 'Tim',
                'name_en' => 'Team',
                'type' => 4,
                'link' => 'team',
                'icon' => 'about-team.png',
                'order' => 10,
                'parent_id' => $about->id
            ]))->save();
        }

        $notExist = DB::table('menus')->where('link', 'partner')->doesntExist();
        if($notExist) {
            (new App\Menu([
                'name_id' => 'Rekanan',
                'name_en' => 'Partner',
                'type' => 5,
                'link' => 'partner',
                'icon' => 'about-partner.png',
                'order' => 20,
                'parent_id' => $about->id
            ]))->save();
        }

        $notExist = DB::table('menus')->where('link', 'contact')->doesntExist();
        if($notExist) {
            (new App\Menu([
                'name_id' => 'Kontak',
                'name_en' => 'Contact',
                'type' => 6,
                'link' => 'contact',
                'icon' => 'about-contact.png',
                'order' => 30,
                'parent_id' => $about->id
            ]))->save();
        }

        //company
        $notExist = DB::table('companies')->where('name', 'PINS Indonesia')->doesntExist();
        if($notExist) {
            (new App\Company([
                'name' => 'PINS Indonesia',
                'email' => 'kontak@pins.co.id',
                'phone' => '0215202560',
                'email_notif' => 'pins.website@gmail.com',
                'address' => 'Jl. HR Rasuna Said kav.C11-C14 Jakarta Selatan',
                'image' => 'pins.jpg',
                'description' => 'PINS Indonesia is a company dealing with the integration of devices and network supported by human resources capability and the best system. As a team, we always focus on the development of innovation to meet the the customers’ need amid the volatile industry situation. We also fully focus on the company transformation in order to be able to win the competition to get the added value for our customers, employees dan shareholers.'
            ]))->save();
        }

        //area
        $notExist = DB::table('areas')->where('name', 'NOC PINS')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'NOC PINS',
                'telp' => '021348444999',
                'fax' => '02134831121',
                'email' => 'helpdesk@pins.co.id',
                'location' => 'Jl. Medan Merdeka Selatan (Monas) Jakarta Selatan',
                'featured' => 'true',
            ]))->save();
        }

        $notExist = DB::table('areas')->where('name', 'MEDAN (Area Sumatera)')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'MEDAN (Area Sumatera)',
                'location' => 'Graha Merah Putih Lt 6 Jl.Putri Hijau No 1, Medan 20111',
                'featured' => 'false',
            ]))->save();
        }

        $notExist = DB::table('areas')->where('name', 'JAKARTA (Area Jabodetabek)')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'JAKARTA (Area Jabodetabek)',
                'location' => 'Jl. Medan Merdeka Selatan (Monas) Jakarta Pusat',
                'featured' => 'false',
            ]))->save();
        }

        $notExist = DB::table('areas')->where('name', 'BANDUNG (Area Jawa Barat)')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'BANDUNG (Area Jawa Barat)',
                'location' => 'Jl. Supratman No 62, Bandung',
                'featured' => 'false',
            ]))->save();
        }

        $notExist = DB::table('areas')->where('name', 'SEMARANG (Area Jawa Tengah)')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'SEMARANG (Area Jawa Tengah)',
                'location' => 'Jl. Pahlawan No 10, Semarang',
                'featured' => 'false',
            ]))->save();
        }

        $notExist = DB::table('areas')->where('name', 'DENPASAR (Area Bali & Nusa Tenggara)')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'DENPASAR (Area Bali & Nusa Tenggara)',
                'location' => 'Graha Merah Putih Lt 6 Jl. Teuku Umar No 6, Denpasar',
                'featured' => 'false',
            ]))->save();
        }

        $notExist = DB::table('areas')->where('name', 'MAKASSAR (Area Sulawesi, Maluku & Papua)')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'MAKASSAR (Area Sulawesi, Maluku & Papua)',
                'location' => 'Jl. A.P Pettarani No 2, Makassar',
                'featured' => 'false',
            ]))->save();
        }

        $notExist = DB::table('areas')->where('name', 'BALIKPAPAN (Area Kalimantan)')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'BALIKPAPAN (Area Kalimantan)',
                'location' => 'Jl. MT Haryono No 169, Balikpapan',
                'featured' => 'false',
            ]))->save();
        }

        $notExist = DB::table('areas')->where('name', 'SURABAYA (Area Jawa Timur)')->doesntExist();
        if($notExist) {
            (new App\Area([
                'name' => 'SURABAYA (Area Jawa Timur)',
                'location' => 'Jl. Sumatra No 131, Surabaya',
                'featured' => 'false',
            ]))->save();
        }

        //social media
        $notExist = DB::table('socialmedia')->where('name', 'facebook')->doesntExist();
        if($notExist) {
            (new App\Socialmedia([
                'name' => 'facebook',
                'link' => 'https://web.facebook.com/pinstheiotcompany',
                'photo' => '-',
            ]))->save();
        }

        $notExist = DB::table('socialmedia')->where('name', 'twitter')->doesntExist();
        if($notExist) {
            (new App\Socialmedia([
                'name' => 'twitter',
                'link' => 'https://twitter.com/pinsindonesia',
                'photo' => '-',
            ]))->save();
        }

        //default user
        $notExist = DB::table('users')->where('email', 'superadmin@gmail.com')->doesntExist();
        if($notExist) {
            (new App\User([
                'name' => 'SuperAdmin',
    	        'email' => 'superadmin@gmail.com',
    	        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
    	        'remember_token' => str_random(10),
    	        'created_at' => '2018-01-01 00:00:00'
            ]))->save();
        }

        //default email template
        $notExist = DB::table('email_templates')->where('name', 'Notif Admin - Contact Us')->doesntExist();
        if($notExist) {
            (new App\Emailtemplate([
                'name' => 'Notif Admin - Contact Us',
                'type' => '1',
                'subject' => 'Contact Us',
                'content' => '<div><p>Pengguna mengirimkan email pada website PINS Indonesia</p><h5>Nama</h5><p>**name**</p><h5>Email</h5><p>**email**</p><h5>Telp</h5><p>**phone**</p><h5>Pesan</h5><p>**message**</p></div>',
                'created_at' => '2018-01-01 00:00:00'
            ]))->save();
        }

        $notExist = DB::table('email_templates')->where('name', 'Notif User - Contact Us')->doesntExist();
        if($notExist) {
            (new App\Emailtemplate([
                'name' => 'Notif User - Contact Us',
                'type' => '1',
                'subject' => 'Contact Us',
                'content' => '<div><p>Halo, **name**</p><p>Terima kasih telah mengirimkan pesan kepada kami, tim kami akan segera menghubungi Anda dalam waktu dekat untuk proses selanjutnya.</p></div>',
                'created_at' => '2018-01-01 00:00:00'
            ]))->save();
        }

        $notExist = DB::table('email_templates')->where('name', 'Notif Admin - Comment')->doesntExist();
        if($notExist) {
            (new App\Emailtemplate([
                'name' => 'Notif Admin - Comment',
                'type' => '2',
                'subject' => 'Comment',
                'content' => '<div><p>Pengguna mengirimkan komentar pada **article** article</p><h5>Nama</h5><p>**name**</p><h5>Email</h5><p>**email**</p><h5>Komentar</h5><p>**comment**</p></div>',
                'created_at' => '2018-01-01 00:00:00'
            ]))->save();
        }

        $notExist = DB::table('email_templates')->where('name', 'Notif User - Comment')->doesntExist();
        if($notExist) {
            (new App\Emailtemplate([
                'name' => 'Notif User - Comment',
                'type' => '2',
                'subject' => 'Comment',
                'content' => '<div><p>Halo, **name**</p><p>Terima kasih telah mengirimkan komentar kepada kami, komentar Anda akan tampil setelah diverifikasi oleh Admin.</p></div>',
                'created_at' => '2018-01-01 00:00:00'
            ]))->save();
        }
    }
}
