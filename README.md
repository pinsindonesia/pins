# Steps for Development Using Docker

1. Install `mysql`, `phpmyadmin`, (*Optional you can skip it*)
   ```
   $ docker-compose -f docker/docker-compose.yml up -d
   ```

   if already exist, start / stop
   ```
   $ docker-composer -f docker/docker-compose.yml start
   $ docker-composer -f docker/docker-compose.yml stop
   ```

2. `phpmyadmin` running on localhost:8080
3. Composer
   ```
   $ composer self-update
   $ composer install
   ```
4. NPM
   ```
   $ npm install
   ```
5. Make sure database `pins_db` exists, import database from `.\pins.sql`
   ```
   $ mysql -h 127.0.0.1 -P 6603 -u pins_db_user -ppins_db_pass pins_db < pins.sql
   ```
6. Storage
   ```shell
   $ php artisan storage:link
   $ composer dump-autoload
   ```
7. Copy `.\example.env` to `.\.env`
8. Generate Encryption Key
   ```
   $ php artisan config:cache
   $ php artisan key:generate
   ```
9.  Run project
    ```shell
    $ php artisan serve
    ```
