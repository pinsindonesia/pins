import React, { Component } from 'react'

import '../lib/easing/easing.min.js'

export default class ButtonScrollToTop extends Component {
    componentDidMount() {
        $(window).scroll(() => {
            if ($(window).scrollTop() > 100) $('.back-to-top').fadeIn('slow')
            else $('.back-to-top').fadeOut('slow')
        })

        $('.back-to-top').click(() => {
            $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo')
            return false;
        })
    }

    render() {
        return (
            <a href='#' className='back-to-top'><i className='fa fa-chevron-up'></i></a>
        );
    }
}
