<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Comment;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	}
	
    public function index()
    {
        return view('pages/comment');
    }

    public function list(Request $request) {
		if( $request->draw != FALSE )   {$draw   = $request->draw;}   else{$draw   = 1;}; 
		if( $request->length != FALSE ) {$length = $request->length;} else{$length = 10;}; 
		if( $request->start != FALSE )  {$start  = $request->start;}  else{$start  = 0;}; 		
		
		$order = $request->order;
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 2;}; 
		$order_fields = array('articles.title_id', 'comments.name', 'comments.created_at');
		
		$search = $request->search;
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		$limit 			= (int) $length;
		$offset			= (int) $start;
		$order_column 	= $order_fields[$order_column];
		$order_dir		= $order_dir;
		$filter 		= $search_value;

    	$list_total = Comment::select('*', 'comments.id')
    							->where('name', 'LIKE', '%'.$filter.'%')
                                ->orWhere('email', 'LIKE', '%'.$filter.'%')
                                ->orWhere('title_id', 'LIKE', '%'.$filter.'%')
                                ->orWhere('title_en', 'LIKE', '%'.$filter.'%')
                                ->leftJoin('articles', 'comments.article_id', '=', 'articles.id')
                                ->get();
    	$list_filtered = Comment::select('*', 'comments.id')
    							->where('name', 'LIKE', '%'.$filter.'%')
                                ->orWhere('email', 'LIKE', '%'.$filter.'%')
                                ->orWhere('title_id', 'LIKE', '%'.$filter.'%')
                                ->orWhere('title_en', 'LIKE', '%'.$filter.'%')
                                ->leftJoin('articles', 'comments.article_id', '=', 'articles.id')
    							->take($limit)
    							->offset($offset)
    							->orderBy($order_column, $order_dir)
    							->get();

    	$result["recordsTotal"] = $list_total->count();
		$result["recordsFiltered"] = $list_total->count();
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list_filtered as $value ) {
			$is_show = $value['showed'] == 'true' ? 'checked' : '';

			$action = '
						<center>
							<button class="btn btn-sm btn-danger" onclick="_delete(\''.$value['id'].'\');" title="Delete"><i class="fa fa-trash"></i></button><br><br>
							<input type="checkbox" id="showed_edit" name="showed_edit" '.$is_show.' onchange="toogle_showed(\''.$value['id'].'\', this.checked)"/> Show
						</center>
					';

			$showed = $value['showed'] == 'true' ? '<br><span class="label label-primary">Showed</span>' : '';

		    array_push($data, 
				array(
					$value['title_id'] . '</br>' . $value['title_en'],
					'<b>Name</b> ' . $value['name'] . '<br/>' . '<b>Email</b> '. $value['email'] . '<br/>' . '<b>Comment</b> '. $value['comment'] . $showed,
                    $action
				)
			);
		}
		
		$result["data"] = $data;

		return response()->json($result);
	}

	public function destroy(Request $request) {
    	$deleted = Comment::where('id', $request->id)->delete();

        if($deleted) {
        	return response()->json([
	            'status' => 'success',
	            'message' => 'Record Deleted.'
	        ]);
        } else {
        	return response()->json([
                'status' => 'error',
                'message' => 'Delete Failed.'
            ]);
        }
    }

    public function update(Request $request) {
        $updated = 0;

        //rules
        $rules = array(
            'id' => 'required',
            'showed' => 'required'
        );
          
        //message error 
        $messages=array(
            //'id.required' => 'ID required.',
        );

        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()) {
            $messages=$validator->messages();
            $errors=$messages->all();

            return response()->json([
                'status' => 'error',
                'message' => $errors[0]
            ]);
        } else {
        	$showed = $request->showed == 'true' ? 'true' : 'false' ;

            $updated = Comment::where('id', $request->id)
                          ->update([
                                'showed' => $showed
                            ]);          
        }

        if($updated) {
            return response()->json([
                'status' => 'success',
                'message' => 'Form Updated.'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Update Failed.'
            ]);
        }
    }
}
