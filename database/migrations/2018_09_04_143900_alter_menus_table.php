<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMenusTable extends Migration
{
    public function up()
    {
        Schema::table('menus', function (Blueprint $table) {
            $table->renameColumn('name', 'name_id');
            $table->string('name_en');
            $table->renameColumn('ref', 'ref_id');
            $table->boolean('featured');
        });
    }

    public function down()
    {

    }
}
