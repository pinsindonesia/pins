<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'layout', 'text_1_id', 'text_2_id', 'text_3_id', 'text_1_en', 'text_2_en', 'text_3_en', 'image_1' , 'image_2', 'image_3', 'video_1', 'name', 'type'
    ];

    protected $dates = ['deleted_at'];

    public function product() {
        return $this->hasOne('App\Product');
    }
}
