@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Product Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add"
                                onclick="resetFormAdd();">Add New <i class="fa fa-plus"></i></a>
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Info
                                </th>
                                <th>
                                    Content
                                </th>
                                <th>
                                    Image
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Modal Add !-->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Product</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Title <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="title_add" name="title_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-2">
                                <input type="checkbox" id="featured_add" name="featured_add" onchange="toogleFormInput('add');" /> Featured
                            </div>
                        </div>
                        <div class="form-group toogledForm_add" id="toogle_overview_add">
                            <label class="control-label col-md-3">Overview 
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" id="overview_id_add" name="overview_id_add">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group toogledForm_add" id="toogle_product_add">
                            <label class="control-label col-md-3">Product 
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" id="product_id_add" name="product_id_add">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group toogledForm_add" id="toogle_whyus_add">
                            <label class="control-label col-md-3">Why Us 
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" id="whyus_id_add" name="whyus_id_add">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group toogledForm_add" id="toogle_cased_add">
                            <label class="control-label col-md-3">Cased 
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" id="cased_id_add" name="cased_id_add">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Excerpt <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <textarea name="excerpt_add" id="excerpt_add" cols="" rows="3" data-required="1" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Image <span class="required">
                            * </span></label>
                            <div class="col-md-8">
                                <img id="uploadPreview_add" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_add" name="image_add" type="file" data-required="1" class="form-control" onchange="PreviewImage_add();"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update!-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Product</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Title <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="title_edit_old" name="title_edit_old" type="hidden" />
                                <input id="title_edit" name="title_edit" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-2">
                                <input id="featured_edit_old" name="featured_edit_old" type="hidden"/>
                                <input type="checkbox" id="featured_edit" name="featured_edit" onchange="toogleFormInput('edit');"/> Featured
                            </div>
                        </div>
                        <div class="form-group toogledForm_edit" id="toogle_overview_edit">
                            <label class="control-label col-md-3">Overview 
                            </label>
                            <div class="col-md-8">
                                <input id="overview_id_edit_old" name="overview_id_edit_old" type="hidden"/>
                                <select class="form-control" id="overview_id_edit" name="overview_id_edit">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group toogledForm_edit" id="toogle_product_edit">
                            <label class="control-label col-md-3">Product 
                            </label>
                            <div class="col-md-8">
                                <input id="product_id_edit_old" name="product_id_edit_old" type="hidden"/>
                                <select class="form-control" id="product_id_edit" name="product_id_edit">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group toogledForm_edit" id="toogle_whyus_edit">
                            <label class="control-label col-md-3">Why Us 
                            </label>
                            <div class="col-md-8">
                                <input id="whyus_id_edit_old" name="whyus_id_edit_old" type="hidden"/>
                                <select class="form-control" id="whyus_id_edit" name="whyus_id_edit">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group toogledForm_edit" id="toogle_cased_edit">
                            <label class="control-label col-md-3">Cased 
                            </label>
                            <div class="col-md-8">
                                <input id="cased_id_edit_old" name="cased_id_edit_old" type="hidden"/>
                                <select class="form-control" id="cased_id_edit" name="cased_id_edit">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Excerpt <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-8">
                                <input id="excerpt_edit_old" name="excerpt_edit_old" type="hidden" />
                                <textarea name="excerpt_edit" id="excerpt_edit" cols="" rows="3" data-required="1" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Image <span class="required">
                            * </span></label>
                            <div class="col-md-8">
                                <img id="uploadPreview_edit" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_edit" name="image_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_edit();"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden" />
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Add Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Confirmation!-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden" />
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type': 'GET',
                'url': 'product/list'
            },
            "columnDefs": [{
                "targets": [1, 2, 3],
                "orderable": false
            }, {
                "targets": [2,3],
                "className": "dt-center"
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth": "40%"}, {"sWidth": "30%"}, {"sWidth": "15%"}, {"sWidth": "15%"}]
        });

        $.ajax({
            type: 'GET',
            url: "product/content_list", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                $("#modal_add #product_id_add").html("");
                $("#modal_add #overview_id_add").html("");
                $("#modal_add #whyus_id_add").html("");
                $("#modal_add #cased_id_add").html("");
                
                $("#modal_edit #product_id_edit").html("");
                $("#modal_edit #overview_id_edit").html("");
                $("#modal_edit #whyus_id_edit").html("");
                $("#modal_edit #cased_id_edit").html("");

                $("#modal_add #product_id_add").append("<option value=\"\">-- choose product content --</option>");
                $("#modal_add #overview_id_add").append("<option value=\"\">-- choose overview content --</option>");
                $("#modal_add #whyus_id_add").append("<option value=\"\">-- choose why us content --</option>");
                $("#modal_add #cased_id_add").append("<option value=\"\">-- choose cased content --</option>");

                $("#modal_edit #product_id_edit").append("<option value=\"\">-- choose product --</option>");
                $("#modal_edit #overview_id_edit").append("<option value=\"\">-- choose overview --</option>");
                $("#modal_edit #whyus_id_edit").append("<option value=\"\">-- choose why us --</option>");
                $("#modal_edit #cased_id_edit").append("<option value=\"\">-- choose cased --</option>");

                for(var item of response.data) {
                    $("#modal_add #product_id_add").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    $("#modal_add #overview_id_add").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    $("#modal_add #whyus_id_add").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    $("#modal_add #cased_id_add").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");

                    $("#modal_edit #product_id_edit").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    $("#modal_edit #overview_id_edit").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    $("#modal_edit #whyus_id_edit").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                    $("#modal_edit #cased_id_edit").append("<option value=\"" + item[0] + "\">" + item[1] + "</option>");
                }
            }
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function toogleFormInput(type) {
        if(type == 'add') {
            $('#modal_add .toogledForm_add').hide();

            $featured = $('#modal_add #featured_add').prop('checked');
            if($featured == true) {
                //$('#modal_add #toogle_overview_add').show();

                //setValidation('add', true);
            } else {
                $('#modal_add #toogle_overview_add').show();
                $('#modal_add #toogle_product_add').show();
                $('#modal_add #toogle_whyus_add').show();
                $('#modal_add #toogle_cased_add').show();

                //setValidation('add', false);
            }
        } else if(type == 'edit') {
            $('#modal_edit .toogledForm_edit').hide();

            $featured = $('#modal_edit #featured_edit').prop('checked');
            if($featured == true) {
                //$('#modal_edit #toogle_overview_edit').show();

                //setValidation('edit', true);
            } else {
                $('#modal_edit #toogle_overview_edit').show();
                $('#modal_edit #toogle_product_edit').show();
                $('#modal_edit #toogle_whyus_edit').show();
                $('#modal_edit #toogle_cased_edit').show();

                //setValidation('edit', false);
            }
        }
    }

    function resetFormAdd() {
        validator_add.resetForm();

        $('#modal_add #title_add').val('');
        $('#modal_add #excerpt_add').val('');
        $('#modal_add #product_id_add').val('');
        $('#modal_add #overview_id_add').val('');
        $('#modal_add #whyus_id_add').val('');
        $('#modal_add #cased_id_add').val('');
        $('#modal_add #image_add').val('');
        $('#modal_add #uploadPreview_add').attr("src", "");
        $('#modal_add #featured_add').prop('checked', false);

        toogleFormInput('add')
    }

    function PreviewImage_add() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_add").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_add").src = oFREvent.target.result;
        };
    };

    function PreviewImage_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_edit").src = oFREvent.target.result;
        };
    };

    function setValidation(type, featured) {
        if(type == 'add') {
            if($featured == true) {
                validator_add = $("#form_add").validate({
                    rules: {
                        title_add: {
                            required: true
                        },
                        overview_id_add: {
                            required: true
                        },
                        excerpt_add: {
                            required: true
                        },
                        image_add: {
                            required: true
                        }
                    },
                    messages: {
                        title_add: {
                            required: "Please enter a title"
                        },
                        overview_id_add: {
                            required: "Please choose a content"
                        },
                        excerpt_add: {
                            required: "Please enter some exerpt"
                        },
                        image_add: {
                            required: "Please choose an image"
                        }
                    },
                    submitHandler: function(form) {
                        $('#confirm_modal_add').modal('toggle');
                    }
                });
            } else {
                validator_add = $("#form_add").validate({
                    rules: {
                        title_add: {
                            required: true
                        },
                        product_id_add: {
                            required: true
                        },
                        overview_id_add: {
                            required: true
                        },
                        whyus_id_add: {
                            required: true
                        },
                        cased_id_add: {
                            required: true
                        },
                        excerpt_add: {
                            required: true
                        },
                        image_add: {
                            required: true
                        }
                    },
                    messages: {
                        title_add: {
                            required: "Please enter a title"
                        },
                        product_id_add: {
                            required: "Please choose a content"
                        },
                        overview_id_add: {
                            required: "Please choose a content"
                        },
                        whyus_id_add: {
                            required: "Please choose a content"
                        },
                        cased_id_add: {
                            required: "Please choose a content"
                        },
                        excerpt_add: {
                            required: "Please enter some exerpt"
                        },
                        image_add: {
                            required: "Please choose an image"
                        }
                    },
                    submitHandler: function(form) {
                        $('#confirm_modal_add').modal('toggle');
                    }
                });
            }
        } else if(type == 'edit') {
            if($featured == true) {
                validator_edit = $("#form_edit").validate({
                    rules: {
                        title_edit: {
                            required: true
                        },
                        overview_id_edit: {
                            required: true
                        },
                        excerpt_edit: {
                            required: true
                        }
                    },
                    messages: {
                        title_edit: {
                            required: "Please enter a title"
                        },
                        overview_id_edit: {
                            required: "Please choose a content"
                        },
                        excerpt_edit: {
                            required: "Please enter some exerpt"
                        }
                    },
                    submitHandler: function(form) {
                        $('#confirm_modal_edit').modal('toggle');
                    }
                });
            } else {
                validator_edit = $("#form_edit").validate({
                    rules: {
                        title_edit: {
                            required: true
                        },
                        product_id_edit: {
                            required: true
                        },
                        overview_id_edit: {
                            required: true
                        },
                        whyus_id_edit: {
                            required: true
                        },
                        cased_id_edit: {
                            required: true
                        },
                        excerpt_edit: {
                            required: true
                        }
                    },
                    messages: {
                        title_edit: {
                            required: "Please enter a title"
                        },
                        product_id_edit: {
                            required: "Please choose a content"
                        },
                        overview_id_edit: {
                            required: "Please choose a content"
                        },
                        whyus_id_edit: {
                            required: "Please choose a content"
                        },
                        cased_id_edit: {
                            required: "Please choose a content"
                        },
                        excerpt_edit: {
                            required: "Please enter some exerpt"
                        }
                    },
                    submitHandler: function(form) {
                        $('#confirm_modal_edit').modal('toggle');
                    }
                });
            }
        }
    }

    function submitForm(action) {
        if(action == 'create') {
            $title           = $('#modal_add #title_add').val();
            $overview        = $('#modal_add #overview_id_add').val();
            $whyus           = $('#modal_add #whyus_id_add').val();
            $cased           = $('#modal_add #cased_id_add').val();
            $product         = $('#modal_add #product_id_add').val();
            $image           = $('#modal_add #image_add');
            $excerpt         = $('#modal_add #excerpt_add').val();
            $featured        = $('#modal_add #featured_add').prop('checked');

            $(".loading").show();

            var form = $('#form_add')[0];
            var fdata = new FormData(form);

            fdata.append("title", $title);
            fdata.append("overview_id", $overview);
            fdata.append("why_us_id", $whyus);
            fdata.append("cased_id", $cased);
            fdata.append("product_id", $product);
            fdata.append("image", $image[0].files[0]);
            fdata.append("excerpt", $excerpt);
            fdata.append("featured", $featured);

            $.ajax({
                type: 'POST',
                url: "product/store", 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fdata, 
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.status == 'success') {
                        $('#confirm_modal_add').modal('toggle');
                        $('#modal_add').modal('toggle');

                        resetFormAdd();

                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                    } else {
                        $('#confirm_modal_add').modal('toggle');

                        $("#modal_message_add").text(response.message);
                        $("#modal_message_add").show();
                        setTimeout(function() { $("#modal_message_add").slideUp(); }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if(action == 'update') {
            $id              = $('#update_id').val();
            $title           = $('#modal_edit #title_edit').val();
            $overview        = $('#modal_edit #overview_id_edit').val();
            $whyus           = $('#modal_edit #whyus_id_edit').val();
            $cased           = $('#modal_edit #cased_id_edit').val();
            $product         = $('#modal_edit #product_id_edit').val();
            $image           = $('#modal_edit #image_edit');
            $excerpt         = $('#modal_edit #excerpt_edit').val();
            $featured        = $('#modal_edit #featured_edit').prop('checked');

            $title_old       = $('#modal_edit #title_edit_old').val();
            $overview_old    = $('#modal_edit #overview_id_edit_old').val();
            $whyus_old       = $('#modal_edit #whyus_id_edit_old').val();
            $cased_old       = $('#modal_edit #cased_id_edit_old').val();
            $product_old     = $('#modal_edit #product_id_edit_old').val();
            $excerpt_old     = $('#modal_edit #excerpt_edit_old').val();
            $featured_old    = $('#modal_edit #featured_edit_old').val() == "true" ? true : false;

            if($title == $title_old && $excerpt == $excerpt_old && $product == $product_old && $overview == $overview_old && $whyus == $whyus_old && $cased == $cased_old && $featured == $featured_old && $image[0].files.length == 0) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $(".loading").show();

                var form = $('#form_edit')[0];
                var fdata = new FormData(form);

                fdata.append("id", $id);
                fdata.append("title", $title);
                fdata.append("overview_id", $overview);
                fdata.append("why_us_id", $whyus);
                fdata.append("cased_id", $cased);
                fdata.append("product_id", $product);
                fdata.append("image", $image[0].files[0]);
                fdata.append("excerpt", $excerpt);
                fdata.append("featured", $featured);

                $.ajax({
                    type: 'POST',
                    url: "product/update", 
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fdata, 
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response.status == 'success') {
                            $('#confirm_modal_edit').modal('toggle');
                            $('#modal_edit').modal('toggle');

                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                        } else {
                            $('#confirm_modal_edit').modal('toggle');

                            $("#modal_message_edit").html(response.message);
                            $("#modal_message_edit").show();
                            setTimeout(function() { $("#modal_message_edit").slideUp(); }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');

        $(".loading").show();

        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "product/destroy",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            },
            success: function (response) {
                if (response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function () {
                        $("#submit_alert_success").slideUp();
                    }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function () {
                        $("#submit_alert_failed").slideUp();
                    }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    function _edit(id) {
        $(".loading").show();

        $.ajax({
            type: 'GET',
            url: "product/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#update_id').val(response.id);

                $('#modal_edit #title_edit').val(response.title);
                $('#modal_edit #excerpt_edit').val(response.excerpt);
                $('#modal_edit #product_id_edit').val(response.product_id);
                $('#modal_edit #overview_id_edit').val(response.overview_id);
                $('#modal_edit #whyus_id_edit').val(response.why_us_id);
                $('#modal_edit #cased_id_edit').val(response.cased_id);
                $('#modal_edit #image_edit').val('');
                $('#modal_edit #uploadPreview_edit').attr("src", "{{asset('storage/product/image')}}".replace("image", response.image));

                response.featured == "true" ? $('#featured_edit').prop('checked', true) : $('#featured_edit').prop('checked', false);

                $('#modal_edit #title_edit_old').val(response.title);
                $('#modal_edit #excerpt_edit_old').val(response.excerpt);
                $('#modal_edit #product_id_edit_old').val(response.product_id);
                $('#modal_edit #overview_id_edit_old').val(response.overview_id);
                $('#modal_edit #whyus_id_edit_old').val(response.why_us_id);
                $('#modal_edit #cased_id_edit_old').val(response.cased_id);
                $('#modal_edit #featured_edit_old').val(response.featured);

                toogleFormInput('edit');

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Add Validation
    $().ready(function() {
        validator_add = $("#form_add").validate({
            rules: {
                title_add: {
                    required: true
                },
                excerpt_add: {
                    required: true
                },
                image_add: {
                    required: true
                }
            },
            messages: {
                title_add: {
                    required: "Please enter a title"
                },
                excerpt_add: {
                    required: "Please enter some exerpt"
                },
                image_add: {
                    required: "Please choose an image"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    //Edit Validation
    $().ready(function() {
        validator_edit = $("#form_edit").validate({
            rules: {
                title_edit: {
                    required: true
                },
                excerpt_edit: {
                    required: true
                }
            },
            messages: {
                title_edit: {
                    required: "Please enter a title"
                },
                excerpt_edit: {
                    required: "Please enter some exerpt"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });

</script>
@endsection
