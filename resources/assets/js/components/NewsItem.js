import React, { Component } from 'react'
import Tilt from 'react-tilt'
import Moment from 'react-moment'

import { Helpers } from '../utils';

export default class NewsItem extends Component {
    render() {
        const { wowDur, wowDelay, href, img, title, content, createdAt, comment } = this.props
        let commentText = comment + ' comment' + (comment && comment > 1 ? 's' : '')
        if (Helpers.getLocale() == 'id') {
            commentText = comment + ' komentar'
        }

        return (
            <div className='col-md-4 col-sm-4 col-xs-12 wow fadeInLeft' data-wow-duration={wowDur} data-wow-delay={wowDelay}>
                <div className='single-blog'>
                    <Tilt className='single-blog-img' options={{scale:1.01}}>
                        <a href={Helpers.fHref(href)}><img className='two-side-rounded-2' src={Helpers.imgUrlNews(img)} alt=''/></a>
                    </Tilt>
                    <div className='blog-meta'>
                        <span className='comments-type'>
                            <i className='fa fa-comment-o'></i>
                            <a href={Helpers.fHref(href)}> {commentText}</a>
                        </span>
                        <span className='date-type'>
                            <i className='fa fa-calendar'></i>
                            <Moment locale={Helpers.getLocale()} fromNow>{createdAt}</Moment>
                        </span>
                    </div>
                    <div className='blog-text'>
                        <h4><a href={Helpers.fHref(href)}>{title}</a></h4>
                        <p dangerouslySetInnerHTML={{ __html: content }} />
                    </div>
                    <span><a href={Helpers.fHref(href)} className='ready-btn'>{Helpers.localeText('read_more')}</a></span>
                </div>
            </div>
        );
    }
}
