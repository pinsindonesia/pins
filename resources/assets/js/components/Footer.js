import React, { Component } from 'react'
import { connect } from 'react-redux'
import Tilt from 'react-tilt'

import { fetchFooter } from '../redux/actions/footerActions';
import { Helpers } from '../utils';

@connect((store) => {
    return {
        contact: store.footer.footer.contact,
        socmed: store.footer.footer.socialMedia,
        menus: store.footer.footer.menus
    }
})
export default class Footer extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.dispatch(fetchFooter())
    }

    render() {
        const { contact, socmed, menus } = this.props

        let menulist = []

        if (menus) {
            const diff = Math.ceil(menus.length/4);
            if (diff > 0) {
                for (let i = 0; i < diff; i++) {
                    let nitems = []
                    for (let j = 0; j < 4; j++) {
                        const nws = menus[(i*4) + j];
                        if (nws) {
                            nitems.push(<FooterMenuItem key={j} title={nws.title} child={nws.child} />)
                        }
                    }
                    menulist.push(<div className='row' key={i}>{nitems}</div>)
                }
            }
        }

        return (
            <div>
                <div className='footer-area'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col-md-3 col-sm-3 col-xs-12'>
                                <div className='footer-content'>
                                    <div className='footer-head'>
                                        <div className='footer-logo'>
                                            <img src={Helpers.fHref('/img/logo.png')} alt='' width={200}/>
                                        </div>

                                        <FooterContact contact={contact} />

                                        <FooterSocMed socmed={socmed} />
                                    </div>
                                </div>
                            </div>

                            <div className='col-md-9 col-sm-9 colxs-12'>
                                <div className='row'>
                                    {menulist.map((v, i) => {
                                        return v
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <FooterCopyright year='2018' company='PINS Indonesia' />
            </div>
        );
    }
}

class FooterContact extends Component {
    render() {
        const { contact } = this.props

        return (
            <div className='footer-contacts'>
                <p><i className='fa fa-phone'></i> {contact && contact.phone}</p>
                <p><i className='fa fa-envelope-o'></i> {contact && contact.mail}</p>
                <p><i className='fa fa-map-marker'></i> {contact && contact.address}</p>
            </div>
        )
    }
}

class FooterSocMed extends Component {
    render() {
        const { socmed } = this.props

        return (
            <div className='footer-icons'>
                <ul>
                    {socmed && socmed.map((sm, i) =>
                        <li key={i}>
                            <Tilt className='Tilt'>
                            <a className={sm.title} href={sm.href}><i className={'Tilt-inner-min fa fa-' + sm.title}></i></a>
                            </Tilt>
                        </li>
                    )}
                </ul>
            </div>
        )
    }
}

class FooterMenuItem extends Component {
    render() {
        const { title, child } = this.props

        return (
            <div className='col-md-3 col-sm-3 col-xs-12'>
                <div className='footer-content'>
                    <div className='footer-head'>
                        <h4>{title}</h4>
                        <ul className="links">
                            {child.map((item, i) =>
                                <li key={i}><a href={Helpers.fHref('/#/nav/' + item.href)}>{item.title}</a></li>
                            )}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

class FooterCopyright extends Component {
    render() {
        const { year, company } = this.props

        return (
            <div className='footer-area-bottom'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-12 col-sm-12 col-xs-12'>
                            <div className='copyright text-center'>
                                <p>{year} &copy; Copyright <strong>{company}</strong>. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
