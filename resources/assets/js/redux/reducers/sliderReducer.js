export default function reducer(state={
    sliders: [],
    fetching: false,
    fetched: false,
    error: null
}, action) {
    switch (action.type) {
        case 'FETCH_SLIDER': {
            return {...state, fetching: true}
        }
        case 'FETCH_SLIDER_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }
        case 'FETCH_SLIDER_FULFILLED': {
            return {...state, fetching: false, fetched: true, sliders: action.payload}
        }
    }

    return state
}
