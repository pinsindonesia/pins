export default function reducer(state={
    language: 'id'
}, action) {
    switch (action.type) {
        case 'SELECT_LANGUAGE': {
            localStorage.setItem('language', action.payload)
            return {...state, language: action.payload}
        }
    }

    return state
}
