import React, { Component } from 'react'

export default class Welcome extends Component {
    render() {
        const { title, subtitle } = this.props

        return (
            <div className='wellcome-area'>
                <div className='well-bg'>
                <div className='test-overly'></div>
                <div className='container'>
                    <div className='row'>
                    <div className='col-md-12 col-sm-12 col-xs-12'>
                        <div className='wellcome-text'>
                        <div className='well-text text-center'>
                            <h2>{title}</h2>
                            <p>{subtitle}</p>
                            <div className='subs-feilds'>
                            <div className='suscribe-input'>
                                <input type='email' className='email form-control width-80' id='sus_email' placeholder='Email'/>
                                <button type='submit' id='sus_submit' className='add-btn width-20'>Subscribe</button>
                                <div id='msg_Submit' className='h3 text-center hidden'></div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}
