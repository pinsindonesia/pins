import Axios from 'axios'
import { Constants, Helpers } from '../../utils';

export function fetchFooter() {
    return (dispatch) => {
        Axios.get(Helpers.localeUrl(Constants.API_FOOTERS_URL))
        .then(res => {
            dispatch({ type: 'FETCH_FOOTER_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'FETCH_FOOTER_REJECTED', payload: err })
        })
    }
}
