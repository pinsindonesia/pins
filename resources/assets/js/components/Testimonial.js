import React, { Component } from 'react'

import '../lib/owlcarousel/owl.carousel.min.js'

import { Helpers } from '../utils'

export default class Testimonial extends Component {
    componentDidMount() {
        this.$testimonial = $(this.refs.testimonial)
        this.$testimonial.owlCarousel({
            loop: true,
            nav: false,
            dots: true,
            autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        })
        // window.startTestimonialCarousel();
    }

    componentWillUnmount() {
        this.$testimonial.owlCarousel('destroy')
    }

    render() {
        return (
            <div className='testimonials-area'>
                <div className='testi-inner area-padding'>
                    <div className='testi-overly'></div>
                    <div className='container '>
                        <div className='row'>
                            <div className='col-md-12 col-sm-12 col-xs-12'>
                                <div className='testimonial-content text-center'>
                                    <a className='quate' href='#'><i className='fa fa-quote-right'></i></a>

                                    <div ref='testimonial' className='owl-carousel testimonial-carousel'>
                                        <TestimonialItem
                                            name='Boby'
                                            content='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget congue.\nconsectetur adipiscing elit. Sed pulvinar luctus est eget congue.' />

                                        <TestimonialItem
                                            name='Jack'
                                            content='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget congue.\nconsectetur adipiscing elit. Sed pulvinar luctus est eget congue.' />

                                        <TestimonialItem
                                            name='Paul'
                                            content='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget congue.\nconsectetur adipiscing elit. Sed pulvinar luctus est eget congue.' />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class TestimonialItem extends Component {
    render() {
        const {name, content} = this.props

        return (
            <div className='single-testi'>
                <div className='testi-text'>
                    <p>{Helpers.nl2br(content)}</p>
                    <h6>{name}</h6>
                </div>
            </div>
        );
    }
}
