import Axios from 'axios'
import { Constants } from '../../utils/index';

export function addEnquiry(name, phone, email, message) {
    return (dispatch) => {
        dispatch({ type: 'ADD_ENQUIRY_PROCESSING' })
        Axios.post(Constants.API_ENQUIRY_URL, {
            name: name,
            phone: phone,
            email: email,
            message: message
        })
        .then(res => {
            dispatch({ type: 'ADD_ENQUIRY_FULFILLED', payload: res.data })
        })
        .catch(err => {
            dispatch({ type: 'ADD_ENQUIRY_REJECTED', payload: err })
        })
    }
}

export function resetEnquiry() {
    return (dispatch) => {
        dispatch({ type: 'ADD_ENQUIRY_RESET' })
    }
}
