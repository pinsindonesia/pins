@extends('layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Article Table</h3>
                </div>
                <div id="submit_alert_success" class="alert alert-success" style="display: none; text-align: center;"></div>
                <div id="submit_alert_failed" class="alert alert-danger" style="display: none; text-align: center;"></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a id="sample_editable_1_new" class="btn btn-success" data-toggle="modal" href="#modal_add"
                                onclick="resetFormAdd();">Add New <i class="fa fa-plus"></i></a>
                            <a href="#" title="Refresh" class="btn btn-default" onclick="ajaxReload();"><i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th>
                                    Content
                                </th>
                                <th>
                                    Media
                                </th>
                                <th>
                                    Info
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Modal Add !-->
<div class="modal fade" id="modal_add" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create Article</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_add' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Title Id<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="title_id_add" name="title_id_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2">Title En<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="title_en_add" name="title_en_add" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">Excerpt Id<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <textarea id="description_short_id_add" name="description_short_id_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">Excerpt En<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <textarea id="description_short_en_add" name="description_short_en_add" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">Description Id<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <textarea id="description_long_id_add" name="description_long_id_add" type="text" data-required="1" class="form-control" rows="6"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">Description En<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <textarea id="description_long_en_add" name="description_long_en_add" type="text" data-required="1" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Image <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <img id="uploadPreview_add" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_add" name="image_add" type="file" data-required="1" class="form-control" onchange="PreviewImage_add();"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Video</label>
                            <div class="col-md-9">
                                <input id="video_add" name="video_add" type="text" data-required="1" class="form-control" placeholder="Enter youtube link"/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <input type="checkbox" id="is_featured_add" name="is_featured_add"/> Featured
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update!-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Article</h4>
            </div>
            <div class="modal-body">
                <div id='modal_message_edit' class="alert alert-danger" style="display: none;"></div>
                <form action="#" id="form_edit" class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Title Id<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="title_edit_id_old" name="title_edit_id_old" type="hidden" />
                                <input id="title_edit_id" name="title_edit_id" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Title En<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="title_edit_en_old" name="title_edit_en_old" type="hidden" />
                                <input id="title_edit_en" name="title_edit_en" type="text" data-required="1" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Excerpt Id<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="description_short_edit_id_old" name="description_short_edit_id_old" type="hidden" />
                                <textarea id="description_short_id_edit" name="description_short_id_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Excerpt En<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="description_short_edit_en_old" name="description_short_edit_en_old" type="hidden" />
                                <textarea id="description_short_en_edit" name="description_short_en_edit" type="text" data-required="1" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Description Id<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="description_long_edit_id_old" name="description_long_edit_id_old" type="hidden" />
                                <textarea id="description_long_id_edit" name="description_long_id_edit" type="text" data-required="1" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Description En<span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <input id="description_long_edit_en_old" name="description_long_edit_en_old" type="hidden" />
                                <textarea id="description_long_en_edit" name="description_long_en_edit" type="text" data-required="1" class="form-control" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Image <span class="required">
                                    * </span>
                            </label>
                            <div class="col-md-9">
                                <img id="uploadPreview_edit" alt="Image" style="width: 100px; height: 100px;" />
                                <input id="image_edit" name="image_edit" type="file" data-required="1" class="form-control" onchange="PreviewImage_edit();"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Video</label>
                            <div class="col-md-9">
                                <input id="video_edit_old" name="video_edit_old" type="hidden" />
                                <input id="video_edit" name="video_edit" type="text" data-required="1" class="form-control" placeholder="Enter link url youtube"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <input id="is_featured_edit_old" name="is_featured_edit_old" type="hidden" />
                                <input type="checkbox" id="is_featured_edit" name="is_featured_edit"/> Featured
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="modal-footer">
                            <div>
                                <input id="update_id" name="update_id" type="hidden" />
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Detail!-->
<div class="modal fade" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detail Article</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Title Id</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="title_detail_id" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>

                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Title En</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="title_detail_en" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Excerpt Id</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7" style="border: 1px solid lightgray; padding: 15px">
                        <label id="description_short_detail_id" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>

                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Excerpt En</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7" style="border: 1px solid lightgray; padding: 15px">
                        <label id="description_short_detail_en" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>

                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Description Id</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7" style="border: 1px solid lightgray; padding: 15px">
                        <label id="description_long_detail_id" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>

                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Description En</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7" style="border: 1px solid lightgray; padding: 15px">
                        <label id="description_long_detail_en" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>

                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Image</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <img id="image_detail" alt="Image" style="width: 300px; height: 250px;"/>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                    
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-2">
                        <label class="label-control"><strong>Video</strong></label>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <label id="video_detail" class="label-control"></label>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
        
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Add Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to submit?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('create');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="confirm_modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to update?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitForm('update');">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Confirmation!-->
<div class="modal fade bs-modal-sm" id="delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                Are you sure want to delete?
            </div>
            <div class="modal-footer">
                <input id="delete_id" name="delete_id" type="hidden" />
                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="_destroy();">Submit</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('assets')
<script>
    var table;
    var validator_add;
    var validator_edit;

    jQuery(document).ready(function () {
        table = $('#table_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                'type': 'GET',
                'url': 'article/list'
            },
            "columnDefs": [{
                "targets": 3,
                "className": "dt-center"
            }, {
                "targets": [1,3],
                "orderable": false
            }],
            "order": [],
            "bAutoWidth": false,
            "aoColumns": [{"sWidth": "40%"}, {"sWidth": "30%"}, {"sWidth": "15%"}, {"sWidth": "15%"}]
        });
    });

    function ajaxReload() {
        table.ajax.reload();
    }

    function resetFormAdd() {
        validator_add.resetForm();

        $('#modal_add #description_short_id_add').unRichText();
        $('#modal_add #description_short_en_add').unRichText();
        $('#modal_add #description_long_id_add').unRichText();
        $('#modal_add #description_long_en_add').unRichText();

        $('#modal_add #title_id_add').val('');
        $('#modal_add #title_en_add').val('');
        $('#modal_add #description_short_id_add').val('');
        $('#modal_add #description_short_en_add').val('');
        $('#modal_add #description_long_id_add').val('');
        $('#modal_add #description_long_id_add').val('');
        $('#modal_add #image_add').val('');
        $('#modal_add #uploadPreview_add').attr("src", "");
        $('#modal_add #video_add').val('');
        $('#modal_add #is_featured_add').prop('checked', false);

        $('#modal_add #description_short_id_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 100,
              videoEmbed: false,
        });

        $('#modal_add #description_short_en_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 100,
              videoEmbed: false,
        });

        $('#modal_add #description_long_id_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 150,
              videoEmbed: false,
        });

        $('#modal_add #description_long_en_add').richText({
              imageUpload: false,
              fileUpload: false,
              height: 150,
              videoEmbed: false,
        });
    }


    function submitForm(action) {
        if(action == 'create') {
            $title_id   = $('#modal_add #title_id_add').val();
            $title_en   = $('#modal_add #title_en_add').val();
            
            $shortdesc_id  = $('#modal_add #description_short_id_add').val();
            $shortdesc_id  = $shortdesc_id == '<div><br></div>' || $shortdesc_id == '<br>' ? '' : $shortdesc_id;

            $shortdesc_en  = $('#modal_add #description_short_en_add').val();
            $shortdesc_en  = $shortdesc_id == '<div><br></div>' || $shortdesc_en == '<br>' ? '' : $shortdesc_en;
            
            $longdesc_id   = $('#modal_add #description_long_id_add').val();
            $longdesc_id   = $longdesc_id == '<div><br></div>' || $longdesc_id == '<br>' ? '' : $longdesc_id;

            $longdesc_en   = $('#modal_add #description_long_en_add').val();
            $longdesc_en   = $longdesc_en == '<div><br></div>' || $longdesc_en == '<br>' ? '' : $longdesc_en;

            $image      = $('#modal_add #image_add');
            $video      = $('#modal_add #video_add').val();
            $feature    = $('#modal_add #is_featured_add').prop('checked');

            $(".loading").show();

            var form = $('#form_add')[0];
            var fdata = new FormData(form);

            fdata.append("title_id", $title_id);
            fdata.append("title_en", $title_en);
            fdata.append("description_short_id", $shortdesc_id);
            fdata.append("description_short_en", $shortdesc_en);
            fdata.append("description_long_id", $longdesc_id);
            fdata.append("description_long_en", $longdesc_en);
            fdata.append("image", $image[0].files[0]);
            fdata.append("video", $video);
            fdata.append("is_featured", $feature);

            $.ajax({
                type: 'POST',
                url: "article/store", 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fdata, 
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.status == 'success') {
                        $('#confirm_modal_add').modal('toggle');
                        $('#modal_add').modal('toggle');

                        resetFormAdd();

                        table.ajax.reload();
                        $("#submit_alert_success").html(response.message);
                        $("#submit_alert_success").show();
                        setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                    } else {
                        $('#confirm_modal_add').modal('toggle');

                        $('#modal_add').animate({ scrollTop: 0 }, 'slow');

                        $("#modal_message_add").text(response.message);
                        $("#modal_message_add").show();
                        setTimeout(function() { $("#modal_message_add").slideUp(); }, 6000);
                    }
                    $(".loading").hide();
                }
            });
        } else if(action == 'update') {
            $id                 = $('#update_id').val();
            $title_id           = $('#modal_edit #title_edit_id').val();
            $title_en           = $('#modal_edit #title_edit_en').val();
            $shortdesc_id       = $('#modal_edit #description_short_id_edit').val();
            $shortdesc_en       = $('#modal_edit #description_short_en_edit').val();
            $longdesc_id        = $('#modal_edit #description_long_id_edit').val();
            $longdesc_en        = $('#modal_edit #description_long_en_edit').val();
            $image              = $('#modal_edit #image_edit');
            $video              = $('#modal_edit #video_edit').val();
            $feature            = $('#modal_edit #is_featured_edit').prop('checked');

            $title_old_id       = $('#modal_edit #title_edit_id_old').val();
            $title_old_en       = $('#modal_edit #title_edit_en_old').val();
            $shortdesc_old_id   = $('#modal_edit #description_short_edit_id_old').val();
            $shortdesc_old_en   = $('#modal_edit #description_short_edit_en_old').val();
            $longdesc_old_id    = $('#modal_edit #description_long_edit_id_old').val();
            $longdesc_old_en    = $('#modal_edit #description_long_edit_en_old').val();
            $video_old          = $('#modal_edit #video_edit_old').val();
            $feature_old        = $('#modal_edit #is_featured_edit_old').val() == "true" ? true : false;

            if($title_id == $title_old_id && $shortdesc_id == $shortdesc_old_id && $shortdesc_en == $shortdesc_old_en && $longdesc_id == $longdesc_old_id && $longdesc_en == $longdesc_old_en && $video == $video_old && $feature == $feature_old && $image[0].files.length == 0) {
                $('#confirm_modal_edit').modal('toggle');
                $('#modal_edit').modal('toggle');
            } else {
                $(".loading").show();

                var form = $('#form_edit')[0];
                var fdata = new FormData(form);

                $shortdesc_id  = $shortdesc_id == '<div><br></div>' || $shortdesc_id == '<br>' ? '' : $shortdesc_id;
                $shortdesc_en  = $shortdesc_en == '<div><br></div>' || $shortdesc_en == '<br>' ? '' : $shortdesc_en;
                $longdesc_id  = $longdesc_id == '<div><br></div>' || $longdesc_id == '<br>' ? '' : $longdesc_id;
                $longdesc_en  = $longdesc_en == '<div><br></div>' || $longdesc_en == '<br>' ? '' : $longdesc_en;

                fdata.append("id", $id);
                fdata.append("title_id", $title_id);
                fdata.append("title_en", $title_en);
                fdata.append("description_short_id", $shortdesc_id);
                fdata.append("description_short_en", $shortdesc_en);
                fdata.append("description_long_id", $longdesc_id);
                fdata.append("description_long_en", $longdesc_en);
                fdata.append("image", $image[0].files[0]);
                fdata.append("video", $video);
                fdata.append("is_featured", $feature);  

                $.ajax({
                    type: 'POST',
                    url: "article/update", 
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fdata, 
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response.status == 'success') {
                            $('#confirm_modal_edit').modal('toggle');
                            $('#modal_edit').modal('toggle');

                            table.ajax.reload();
                            $("#submit_alert_success").html(response.message);
                            $("#submit_alert_success").show();
                            setTimeout(function() { $("#submit_alert_success").slideUp(); }, 6000);
                        } else {
                            $('#confirm_modal_edit').modal('toggle');

                            $('#modal_edit').animate({ scrollTop: 0 }, 'slow');

                            $("#modal_message_edit").html(response.message);
                            $("#modal_message_edit").show();
                            setTimeout(function() { $("#modal_message_edit").slideUp(); }, 6000);
                        }
                        $(".loading").hide();
                    }
                });
            }
        }
    }

    function PreviewImage_add() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_add").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_add").src = oFREvent.target.result;
        };
    };

    function PreviewImage_edit() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image_edit").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview_edit").src = oFREvent.target.result;
        };
    };

    function _delete(id) {
        $('#delete_modal').modal('toggle');
        $('#delete_id').val(id);
    }

    function _destroy(id) {
        $('#delete_modal').modal('toggle');
        $(".loading").show();
        $id = $('#delete_id').val();

        $.ajax({
            type: 'DELETE',
            url: "article/destroy",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $id
            },
            success: function (response) {
                if (response.status == 'success') {
                    table.ajax.reload();
                    $("#submit_alert_success").html(response.message);
                    $("#submit_alert_success").show();
                    setTimeout(function () {
                        $("#submit_alert_success").slideUp();
                    }, 6000);
                } else {
                    table.ajax.reload();
                    $("#submit_alert_failed").html(response.message);
                    $("#submit_alert_failed").show();
                    setTimeout(function () {
                        $("#submit_alert_failed").slideUp();
                    }, 6000);
                }
                $(".loading").hide();
            }
        });
    }

    function _edit(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "article/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#modal_edit #description_short_id_edit').unRichText();
                $('#modal_edit #description_short_en_edit').unRichText();
                $('#modal_edit #description_long_id_edit').unRichText();
                $('#modal_edit #description_long_en_edit').unRichText();

                $('#modal_edit #description_short_id_edit').val('');
                $('#modal_edit #description_short_en_edit').val('');
                $('#modal_edit #description_long_id_edit').val('');
                $('#modal_edit #description_long_en_edit').val('');

                $('#update_id').val(response.id);
                $('#modal_edit #title_edit_id').val(response.title_id);
                $('#modal_edit #title_edit_en').val(response.title_en);
                $('#modal_edit #description_short_id_edit').val(response.description_short_id);
                $('#modal_edit #description_short_en_edit').val(response.description_short_en);
                $('#modal_edit #description_long_id_edit').val(response.description_long_id);
                $('#modal_edit #description_long_en_edit').val(response.description_long_en);
                $('#modal_edit #video_edit').val(response.video);
                $('#modal_edit #image_edit').val('');
                $('#modal_edit #uploadPreview_edit').attr("src", "{{asset('storage/article/image')}}".replace("image", response.image));

                response.is_featured == "true" ? $('#is_featured_edit').prop('checked', true) : $('#is_featured_edit').prop('checked', false);

                $('#modal_edit #title_edit_id_old').val(response.title_id);
                $('#modal_edit #title_edit_en_old').val(response.title_en);
                $('#modal_edit #description_short_edit_id_old').val(response.description_short_id);
                $('#modal_edit #description_short_edit_en_old').val(response.description_short_en);
                $('#modal_edit #description_long_edit_id_old').val(response.description_long_id);
                $('#modal_edit #description_long_edit_en_old').val(response.description_long_en);
                $('#modal_edit #video_edit_old').val(response.video);
                $('#modal_edit #is_featured_edit_old').val(response.is_featured);

                $('#modal_edit #description_short_id_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 100,
                      videoEmbed: false,
                });

                $('#modal_edit #description_short_en_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 100,
                      videoEmbed: false,
                });

                $('#modal_edit #description_long_id_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 150,
                      videoEmbed: false,
                });

                $('#modal_edit #description_long_en_edit').richText({
                      imageUpload: false,
                      fileUpload: false,
                      height: 150,
                      videoEmbed: false,
                });

                $('#modal_edit').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    function _detail(id) {
        $(".loading").show();
        $.ajax({
            type: 'GET',
            url: "article/detail",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': id
            },
            success: function (response) {
                $('#modal_detail #title_detail_id').text(response.title_id);
                $('#modal_detail #title_detail_en').text(response.title_en);
                $('#modal_detail #description_short_detail_id').html(response.description_short_id);
                $('#modal_detail #description_short_detail_en').html(response.description_short_en);
                $('#modal_detail #description_long_detail_id').html(response.description_long_id);
                $('#modal_detail #description_long_detail_en').html(response.description_long_en);
                $('#modal_detail #image_detail').attr("src", "{{asset('storage/article/image')}}".replace("image", response.image));
                $('#modal_detail #video_detail').text(response.video);
                
                $('#modal_detail').modal('toggle');

                $(".loading").hide();
            }
        });
    }

    //Add Validation
    $().ready(function() {
        validator_add = $("#form_add").validate({
            rules: {
                title_id_add: {
                    required: true
                },
                title_en_add: {
                    required: true
                },
                image_add: {
                    required: true
                }
            },
            messages: {
                title_id_add: {
                    required: "Please enter a title ID"
                },
                title_en_add: {
                    required: "Please enter a title EN"
                },
                image_add: {
                    required: "Please choose an image"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_add').modal('toggle');
            }
        });
    });

    //Edit Validation
    $().ready(function() {
        validator_edit = $("#form_edit").validate({
            rules: {
                title_edit_id: {
                    required: true
                },
                title_edit_en: {
                    required: true
                }
            },
            messages: {
                title_edit_id: {
                    required: "Please enter a title ID"
                },
                title_edit_en: {
                    required: "Please enter a title EN"
                }
            },
            submitHandler: function(form) {
                $('#confirm_modal_edit').modal('toggle');
            }
        });
    });

</script>
<style>
    
#modal_add, #modal_edit {
  overflow: auto !important;
}

</style>
@endsection
